import torch
DEVICE = 'cuda:0' if torch.cuda.is_available() else "cpu"
D_IDX = 0   # 0 for embedding state, 1 for dispatching state
# DEVICE = "cpu"


class Configs(object):
  class MEnum(object):
    @classmethod
    def has_value(cls, value):
      members = [
        getattr(cls, attr) for attr in dir(cls)
        if not callable(getattr(cls, attr)) and not attr.startswith("__")
      ]
      return value in members

    @classmethod
    def get_members(cls):
      members = [
        getattr(cls, attr) for attr in dir(cls)
        if not callable(getattr(cls, attr)) and not attr.startswith("__")
      ]
      return members

    @classmethod
    def to_dict(cls):
      _dict = {
        attr: getattr(cls, attr) for attr in dir(cls)
        if not callable(getattr(cls, attr)) and not attr.startswith("__")
      }
      return _dict

  class SACConfigBase(MEnum):
    is_continuous_action = False
    action_range = [-3, 3]
    critic_range = [None, None]
    device = DEVICE
    hidden_dim = 128
    hidden_depth = 2
    actor_temp = 2.2
    discount = 0.99
    init_temperature = 0.1
    alpha_lr = 1e-3
    alpha_betas = [0.9, 0.999]
    actor_lr = 1e-3
    actor_betas = [0.9, 0.999]
    critic_lr = 1e-3
    critic_betas = [0.9, 0.999]
    critic_tau = 0.05
    critic_target_update_frequency = 1
    batch_size = 128
    learnable_temperature = True
    epsilon = 1e-5
    model_path = "./checkpoints/model.pt"
    target_entropy = None

  class SACDConfigBase(MEnum):
    critic_range = [None, None]
    device = DEVICE
    hidden_dim = 128
    hidden_depth = 2
    discount = 0.99
    init_temperature = 0.1
    alpha_lr = 1e-4
    alpha_betas = [0.9, 0.999]
    actor_lr = 1e-4
    actor_betas = [0.9, 0.999]
    critic_lr = 1e-4
    critic_betas = [0.9, 0.999]
    critic_tau = 0.05
    critic_target_update_frequency = 1
    batch_size = 128
    learnable_temperature = True
    epsilon = 1e-5
    model_path = "./checkpoints/model.pt"
    target_entropy = None

  class TD3MasterConfig(MEnum):
    critic_range = [None, None]
    device = DEVICE
    d_model = 32
    hidden_depth = 4
    actor_temp = 2.2
    discount = 0.5
    actor_lr = 3e-4
    actor_betas = [0.9, 0.999]
    critic_lr = 3e-4
    critic_betas = [0.9, 0.999]
    tau = 0.005
    policy_update_frequency = 1
    batch_size = 128
    epsilon = 1e-5
    model_path = "./checkpoints/model.pt"
    num_embeddings = 50
    num_heads = 4
    dff = 64
    dropout = 0.

  class DQNConfig(MEnum):
    device = DEVICE
    action_dim = 6
    d_model = 30
    hidden_depth = 5
    batch_size = 32
    discount = 0.9
    tau = 0.01
    eps = [0.5, 0.1]
    eps_decay = 300
    critic_range = [None, None]
    critic_lr = 1e-4
    critic_betas = [0.9, 0.999]
    epsilon = 1e-5
    model_path = "./compare/model.pt"

  class ReplayBufferConfig(MEnum):
    capacity = int(5e3)
    device = DEVICE
    eta_init = 0.996
    eta_term = 1.0
    c_min = 5000
    beta_1 = 0.6
    beta_2 = 0.6
    buffer_path = "./checkpoints/buffer.pkl"

  class RNDConfig(MEnum):
    hidden_dims = [64, 64]
    output_dim = 64
    rnd_lr = 1e-5

  class RIDEConfig(MEnum):
    ride_lr = 1e-5
    d_model = 64

  class GAEConfig(MEnum):
    gamma = 0.5
    lambda_ = 0.95

  class ScalerConfig(MEnum):
    intrinsic_scaler_path = "./checkpoints/int_scaler.pth"
    reward_scaler_path = "./checkpoints/reward_scaler.pth"

  class WarmUpConfig(MEnum):
    pretrain_path = "./checkpoints/pre_scheduler.pt"  # only for scheduling
    pre_iteration = 50
    pre_epoch = 500
    load_model = False

  class ShortestPathConfig(MEnum):
    two = False
    is_obstacles = True

  class CombinationConfig(MEnum):
    penalty = -1
    final_reward = 1
    goal = [2, 5, 3]

  class JobShopConfig(MEnum):
    m_type = [4, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 8, 6, 2, 4, 2, 2, 4, 2, 4, 4, 4, 4]
    j_type = {
      'type0': [0, 1, 12, 13, 22, 14, 19, 21, 22, 21,
                22, 21, 16, 12, 13, 14, 22, 15, 23, 22,
                21, 16, 0, 7, 3, 21, 21, 0, 1, 7,
                12, 13, 17, 22, 14, 15, 22, 17, 21, 0,
                0, 12, 13, 22, 14, 15, 23, 22, 21, 16,
                0, 1, 7, 8, 20, 21, 0, 3, 21, 21,
                0, 1, 12, 13, 22, 14, 15, 23, 23, 22,
                21, 16, 23, 0, 1, 6, 0, 2, 21, 12,
                14, 22, 21, 21, 21, 16, 12, 13, 17, 22,
                14, 15, 19, 22, 0, 16, 0, 0, 2, 12,
                13, 15, 23, 22, 21, 16, 8, 20, 0, 2,
                12, 13, 14, 22, 14, 15, 23, 22, 21, 16,
                0, 2, 9, 21, 11, 5, 21, 5, 0, 0,
                3, 9, 18, 22, 0, 9, 12, 13, 15, 20,
                11, 12, 13, 17, 22, 14, 14, 14, 15, 18,
                22, 21, 16, 10, 12, 13, 14, 20, 22, 4]
    }
    n_jobs = 10
    n_initial_jobs = 30
    stocker_capacity = 20
    _beta = 10
    verbose = False
    len_dispatching_state = 9
    len_compare_state = 7
    len_j_state = [3, len_dispatching_state][D_IDX]
    len_m_state = 2
    len_state_seq = [2, 1][D_IDX]

  class TrainConfig(MEnum):
    myseed = 304
    n_episode = 10000
    print_stats_frequency = 1
    save_frequency = 10
    is_wandb = True
    debug_frequency = 500     # if 0, no debugging
    plot_frequency = 100      # only for shortest path

  class ExperimentConfig(MEnum):
    is_static_scheduling = False    # scheduling mode: static or dynamic
    set_torch_seed = False          # if True, explore barely
    fix_seed = True
    curiosity_mode = ["no_curiosity", "rnd", "ride"][1]     # 1 is best
    environment = ["path", "combination", "jobshop"][2]
    agent_mode = ["sac", "td3", "sacd", "dqn"][1]           # dqn is comparison method
    is_credit_assignment = True     # if False, no delayed reward
    is_scaling_reward = False       # False is best
    int_reward_clipping = 1.0       # if None, no clipping (sensitive parameter for convergence)
    sampling_mode = ["RANDOM", "PER", "PER_ERE"][0]     # 0 is best
    use_gae = True
    clipping_gradient = False       # only for actor in TD3
    epi_without_scaling = 0         # when scaling reward
    use_dispatching_state = False
    is_state_embedding = True
    use_compare_state = False
    do_warmup = False
    do_gantt_plot = False
    is_loading = False
    use_semiconductor_fab = True

  class WanDBConfig(MEnum):
    PROJECT = "rl_project_v1.14"
    NAME = "scheduler"
    PATH = "mywandb.h5"
    MODE = "compare"

  class RLAgent(MEnum):
    SAC = "sac"
    TD3 = "td3"
    SACD = "sacd"
    DQN = "dqn"

  class DispatchingRules(MEnum):
    RANDOM = 0
    FIFO = 1
    MST = 2
    SPT = 3
    EDD = 4
    CR = 5
    MDD = 6
    ATC = 7

  class InputRules(MEnum):
    POISS = 0
    DETERMIN = 1
    CL = 2
