import pdb
from datetime import datetime
from config import Configs
from envs import JobShopScheduling
from agent import TD3Agent, DispatchingRules, DQNAgent


def runEnv(scheduler):
  done = 0
  env.reset()

  while done < 1:
    done = env.next(scheduler)[-1]

  return env.c_max, env.f_bar, env.t_bar


if __name__ == "__main__":
  # Environment
  myseed = Configs.TrainConfig.myseed
  env = JobShopScheduling(myseed=myseed, **Configs.JobShopConfig.to_dict())

  # Dispatching rules
  rand = DispatchingRules(len_j_state=env.len_dispatching_state,
                          mode=Configs.DispatchingRules.RANDOM)
  fifo = DispatchingRules(len_j_state=env.len_dispatching_state,
                          mode=Configs.DispatchingRules.FIFO)
  spt = DispatchingRules(len_j_state=env.len_dispatching_state,
                        mode=Configs.DispatchingRules.SPT)
  mst = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.MST)
  edd = DispatchingRules(len_j_state=env.len_dispatching_state,
                          mode=Configs.DispatchingRules.EDD)
  cr = DispatchingRules(len_j_state=env.len_dispatching_state,
                        mode=Configs.DispatchingRules.CR)
  mdd = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.MDD)
  atc = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.ATC)

  # Reinforcement learning agent
  do_compare = False
  if not do_compare:
    agent = TD3Agent(obs_dim=env.observation_shape[0],
                     action_dim=env.action_shape[0],
                     len_global_state=env.len_global_state,
                     len_j_state=env.len_j_state,
                     len_unit_state=env.len_unit_state,
                     **Configs.TD3MasterConfig.to_dict())
  else:
    agent = DQNAgent(obs_dim=env.observation_shape[0],
                     len_dispatching_state=env.len_dispatching_state,
                     len_compare_state=env.len_compare_state,
                     **Configs.DQNConfig.to_dict())

  # Load
  agent.load_model()

  # Run
  res = []
  actor_names = ["RL", "RANDOM", "FIFO", "SPT", "MST", "EDD", "CR", "MDD", "ATC"]
  actors = [agent, rand, fifo, spt, mst, edd, cr, mdd, atc]
  actor_names = ["TD3-GAE"]
  actors = [agent]
  for actor, actor_name in zip(actors, actor_names):
    tic = datetime.now()
    ret = runEnv(actor)
    res.append(ret)
    print("[%s]" % actor_name)
    print("  - elapsed time     : %s" % (datetime.now() - tic))
    print("  - cmax, fbar, tbar : %d, %.2f, %.2f" % ret)
  pdb.set_trace()