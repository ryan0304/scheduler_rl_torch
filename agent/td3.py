import torch
import copy
import numpy as np

from models import EmbeddingWrapper, ETCEEncoder
from agent.critic import DoubleQCriticMaster
from agent.actor import MultinomialPolicyMaster
from utils import to_np
from .utils import Agent, soft_update_params


class TD3Agent(Agent):
  """Twin Delayed Deep Deterministic Policy Gradient with TD error
  [1] https://spinningup.openai.com/en/latest/algorithms/td3.html
  [2] https://github.com/sfujim/TD3/blob/master/TD3.py
  """
  training: bool

  def __init__(self, obs_dim, action_dim, len_global_state, len_j_state, len_unit_state,
               critic_range, device, d_model, hidden_depth,
               actor_temp, discount, actor_lr, actor_betas, critic_lr, critic_betas,
               tau, policy_update_frequency, batch_size, epsilon, model_path,
               num_embeddings, num_heads, dff, dropout):
    super(TD3Agent, self).__init__(obs_dim, action_dim, torch.device(device))

    self.device = torch.device(device)
    self.discount = discount
    self.tau = tau
    self.policy_update_frequency = policy_update_frequency
    self.batch_size = batch_size
    self.epsilon = epsilon
    self.model_path = model_path
    self.temp = torch.tensor(actor_temp).to(self.device)

    if not self.is_state_embedding:
      self.critic = DoubleQCriticMaster(
        self.is_state_embedding, action_dim, critic_range,
        obs_dim=obs_dim, hidden_depth=hidden_depth, d_model=d_model
      ).to(self.device)
      self.actor = MultinomialPolicyMaster(
        self.is_state_embedding, action_dim, self.temp,
        obs_dim=obs_dim, hidden_depth=hidden_depth, d_model=d_model
      ).to(self.device)

    elif self.environment == "path":
      self.embedder = embedder = EmbeddingWrapper(d_model)
      self.critic = DoubleQCriticMaster(
        self.is_state_embedding, action_dim, critic_range,
        embedder=embedder
      ).to(self.device)
      self.actor = MultinomialPolicyMaster(
        self.is_state_embedding, action_dim, self.temp,
        embedder=embedder
      ).to(self.device)

    elif self.environment == "jobshop":
      self.etce_encoder = etce_encoder = ETCEEncoder(
        len_global_state, len_j_state, len_unit_state, num_embeddings,
        d_model, num_heads, dff, hidden_depth, dropout)
      self.critic = DoubleQCriticMaster(
        self.is_state_embedding, action_dim, critic_range,
        etce_encoder=etce_encoder, d_model=d_model
      ).to(self.device)
      self.actor = MultinomialPolicyMaster(
        self.is_state_embedding, action_dim, self.temp,
        etce_encoder=etce_encoder, len_unit_state=len_unit_state,
        len_j_state=len_j_state, d_model=d_model
      ).to(self.device)

    else:
      raise NotImplemented

    self.critic_target = copy.deepcopy(self.critic)
    self.actor_target = copy.deepcopy(self.actor)

    # optimizers
    self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                            lr=actor_lr,
                                            betas=actor_betas)

    self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                             lr=critic_lr,
                                             betas=critic_betas)

    self.train()
    self.critic_target.train()
    self.actor_target.train()

  def train(self, training=True):
    self.training = training
    self.actor.train(training)
    self.critic.train(training)

  def select_action(self, obs, sample=False, cnt_feasible=None):
    obs = torch.FloatTensor(obs).to(self.device)
    obs = obs.unsqueeze(0)
    if cnt_feasible is None:
      dist = self.actor(obs)
    else:
      dist = self.actor(obs, cnt_feasible)
    action = self.actor.hard_sample(dist)
    assert action.ndim == 2 and action.shape[0] == 1
    return to_np(action[0])

  def update_critic(self, obs, action, reward, next_obs, not_done, weight, ret):
    GAE_VERSION = {'ori': 0, 'adv': 1, 'ad2': 2}['adv']
    with torch.no_grad():
      # Compute the target Q value
      if self.use_gae:
        target_1, target_2 = self.critic_target.calc_value(next_obs)
      else:
        # Rsample next action
        dist = self.actor_target(next_obs)
        next_action = dist.rsample()
        target_1, target_2 = self.critic_target(next_obs, next_action)
      target_V = torch.min(target_1, target_2)
      target_Q = reward + not_done * self.discount * target_V

    # Get current Q estimates
    current_Q1, current_Q2 = self.critic(obs, action)
    if self.use_gae and GAE_VERSION in [1, 2]:
      td_error1 = current_Q1 - ret
      td_error2 = current_Q2 - ret
    else:
      td_error1 = current_Q1 - target_Q
      td_error2 = current_Q2 - target_Q

    critic_loss = (td_error1.pow(2) * weight).mean() + (td_error2.pow(2) * weight).mean()

    # Get value loss
    value_loss = torch.tensor(0, device=obs.device)
    if self.use_gae:
      current_V1, current_V2 = self.critic.calc_value(obs)
      if GAE_VERSION == 2:
        v_error1 = current_V1 - target_Q
        v_error2 = current_V2 - target_Q
      else:
        v_error1 = current_V1 - ret
        v_error2 = current_V2 - ret
      value_loss = (v_error1.pow(2) * weight).mean() + (v_error2.pow(2) * weight).mean()
      critic_loss += value_loss

    # Optimize the critic
    self.critic_optimizer.zero_grad()
    critic_loss.backward()
    self.critic_optimizer.step()

    # TD error
    TD_error = 0.5 * (torch.abs(td_error1) + torch.abs(td_error2)) + self.epsilon

    # Q values
    Q_value = 0.5 * (current_Q1.mean() + current_Q2.mean())

    return to_np(critic_loss), to_np(TD_error), to_np(Q_value), to_np(value_loss)

  def update_actor(self, obs, weight):
    # select action
    dist = self.actor(obs)
    action = dist.probs

    # Compute actor loss
    if not self.use_gae:
      actor_loss = -(self.critic.compute_Q1(obs, action) * weight).mean()
    else:
      actor_loss = -((self.critic.compute_Q1(obs, action) - self.critic.compute_V1(obs)) * weight).mean()

    # optimize the actor
    self.actor_optimizer.zero_grad()
    actor_loss.backward()
    if self.clipping_gradient:
      torch.nn.utils.clip_grad_norm(self.actor.parameters(), max_norm=0.5)
    self.actor_optimizer.step()

    return to_np(actor_loss)

  def update(self, replay_buffer, iteration, n_epi, k, n_step):
    obs, action, reward, next_obs, not_done, weight, ret, idxs = replay_buffer.sample(
      self.batch_size, n_epi, k, n_step)

    critic_loss, prios, q_value, value_loss = self.update_critic(
      obs, action, reward, next_obs, not_done, weight, ret)

    actor_loss = np.nan
    if iteration % self.policy_update_frequency == 0:
      actor_loss = self.update_actor(obs, weight)

      # Update the frozen target models
      soft_update_params(self.critic, self.critic_target, self.tau)
      soft_update_params(self.actor, self.actor_target, self.tau)

    replay_buffer.update_priority(idxs, prios)

    int_loss = np.nan
    if self.curiosity_mode == "rnd":
      int_loss = self.curiosity.update(next_obs)
    elif self.curiosity_mode == "ride":
      int_loss = self.curiosity.update(obs, action, next_obs)

    return critic_loss, actor_loss, int_loss, value_loss, q_value

  def save_model(self, iteration, model_path=None):
    if not model_path:
      model_path = self.model_path
    torch.save({
      "critic_state_dict": self.critic.state_dict(),
      "actor_state_dict": self.actor.state_dict(),
      "optimizer_critic_state_dict": self.critic_optimizer.state_dict(),
      "optimizer_actor_state_dict": self.actor_optimizer.state_dict(),
      "iteration": iteration
    }, model_path)
    self.int_scaler.save()
    self.r_scaler.save()

  def load_model(self, model_path=None):
    if not model_path:
      model_path = self.model_path
    checkpoint = torch.load(model_path, map_location=self.device)
    self.critic.load_state_dict(checkpoint['critic_state_dict'])
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.actor.load_state_dict((checkpoint['actor_state_dict']))
    self.actor_target.load_state_dict(self.actor.state_dict())
    self.critic_optimizer.load_state_dict(checkpoint['optimizer_critic_state_dict'])
    self.actor_optimizer.load_state_dict(checkpoint['optimizer_actor_state_dict'])
    self.int_scaler.load()
    self.r_scaler.load()
    print('[ load model ] iteration=%d' % checkpoint["iteration"])