import numpy as np
import torch
import _pickle as pickle

from config import Configs


class ReplayBuffer(object):
  """Buffer to store environment transitions.
  + ERE (Emphasizing Recent Experience)
  + PER (Prioritized Experience Replay)
  """

  def __init__(self, obs_shape, action_shape, n_episode,
               capacity, device,
               eta_init, eta_term, c_min, beta_1, beta_2,
               buffer_path):
    self.capacity = int(capacity)
    self.device = device
    self.sampling_mode = Configs.ExperimentConfig.sampling_mode
    self.use_gae = Configs.ExperimentConfig.use_gae

    # Parameters related to ERE (Emphasizing Recent Experience)
    self.eta_init = eta_init
    self.eta_term = eta_term
    self.n_episode = n_episode
    self.c_min = c_min

    # Parameters related to PER (Prioritized Experience Replay)
    self.beta_1 = beta_1
    self.beta_2 = beta_2

    # Save class using pickle
    self.buffer_path = buffer_path

    self.obses = np.empty((self.capacity, *obs_shape), dtype=np.float32)
    self.next_obses = np.empty((self.capacity, *obs_shape), dtype=np.float32)
    self.actions = np.empty((self.capacity, *action_shape), dtype=np.float32)
    self.rewards = np.empty((self.capacity, 1), dtype=np.float32)
    self.not_dones = np.empty((self.capacity, 1), dtype=np.float32)
    self.priorities = np.empty((self.capacity, 1), dtype=np.float32)
    self.returns = np.empty((self.capacity, 1), dtype=np.float32)

    self.idx = 0
    self.full = False
    self.max_priority = np.array([0.], dtype=np.float32)

  def __len__(self):
    return self.capacity if self.full else self.idx

  def _get_eta_t(self, t):
    return self.eta_init + (self.eta_term - self.eta_init) * t / self.n_episode

  def _get_c_k(self, t, k, n_step):
    eta_t = self._get_eta_t(t)
    return max(int(self.__len__() * eta_t ** (k * 1000 / n_step)), self.c_min)

  def add(self, obs, action, reward, next_obs, done, ret=None):
    if self.use_gae and ret is None:
      raise ValueError("If GAE mode, ret is necessary variables!")

    np.copyto(self.obses[self.idx], obs)
    np.copyto(self.actions[self.idx], action)
    np.copyto(self.rewards[self.idx], [reward])
    np.copyto(self.next_obses[self.idx], next_obs)
    np.copyto(self.not_dones[self.idx], [not done])
    np.copyto(self.priorities[self.idx], self.max_priority)
    if self.use_gae:
      np.copyto(self.returns[self.idx], [ret])

    self.idx = (self.idx + 1) % self.capacity
    self.full = self.full or self.idx == 0
    return

  def sample(self, batch_size, t, k, n_step):
    if self.sampling_mode == "RANDOM":
      idxs = np.random.randint(0, self.__len__(), size=batch_size)
      weights = np.ones((batch_size, 1))
    else:
      N = self.__len__()

      # Emphasizing Recent Experience
      if not self.sampling_mode == "PER_ERE":
        d_c_k = np.random.randint(0, self.__len__(), size=batch_size)

      else:
        c_k = min(self._get_c_k(t, k, n_step), N)
        c_idx = self.idx - 1
        if c_k == N:
          d_c_k = np.arange(N)
        elif c_idx - c_k < 0:
          d_c_k = np.append(np.arange(N-c_k+c_idx, N), np.arange(c_idx))
        else:
          d_c_k = np.arange(c_idx-c_k, c_idx)

      # Prioritized Experience Replay
      prios = self.priorities[d_c_k]
      prios = np.ones(prios.shape) if prios.sum() == 0 else np.power(prios, self.beta_1)
      prios /= prios.sum()
      sample_idxs = np.random.choice(len(prios), size=batch_size, p=prios.reshape(-1))
      idxs = d_c_k[sample_idxs]

      # Compute importance sampling weight
      weights = np.power(self.capacity * prios[sample_idxs], -self.beta_2)
      weights /= weights.max()

    # Sampling
    obses = torch.as_tensor(self.obses[idxs], device=self.device).float()
    actions = torch.as_tensor(self.actions[idxs], device=self.device)
    rewards = torch.as_tensor(self.rewards[idxs], device=self.device)
    next_obses = torch.as_tensor(self.next_obses[idxs], device=self.device).float()
    not_dones = torch.as_tensor(self.not_dones[idxs], device=self.device)
    weights = torch.as_tensor(weights, device=self.device)
    returns = torch.as_tensor(self.returns[idxs], device=self.device)

    return obses, actions, rewards, next_obses, not_dones, weights, returns, idxs

  def update_priority(self, idxs, prios):
    for idx, prio in zip(idxs, prios):
      np.copyto(self.priorities[idx], prio)
    self.max_priority = max(self.max_priority, max(prios))
    return

  def save(self, filename=None):
    filename = filename if filename else self.buffer_path
    with open(filename, 'wb') as f:
      f.write(pickle.dumps(self.__dict__))

  def load(self, filename=None):
    filename = filename if filename else self.buffer_path
    with open(filename, 'rb') as f:
      data_pickle = f.read()
    self.__dict__ = pickle.loads(data_pickle)