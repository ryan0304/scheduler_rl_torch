import numpy as np


class CompositeRules(object):
  """Composite rules"""

  def __init__(self, len_dispatching_state):
    super().__init__()

    self.len_dispatching_state = len_dispatching_state

  def select_action(self, dispatching_state, rule_idx):
    output_dim = int(len(dispatching_state) / self.len_dispatching_state)
    pad_dim = int(sum(np.isinf(dispatching_state)) / self.len_dispatching_state)
    real_dim = output_dim - pad_dim

    j_states = np.reshape(dispatching_state, (output_dim, -1))[:real_dim]
    j_states = np.column_stack((
      j_states[:, 7],
      j_states[:, 3]+j_states[:, 7],
      j_states[:, 4],
      j_states[:, 6],
      j_states[:, 5]
    ))    # (OP_i, n_i, D_i, sum(t_ij), now)

    if rule_idx == 0:
      idx = self._1st_rule(j_states)
    elif rule_idx == 1:
      idx = self._2nd_rule(j_states)
    elif rule_idx == 2:
      idx = self._3rd_rule(j_states)
    elif rule_idx == 3:
      idx = self._4th_rule(real_dim)
    elif rule_idx == 4:
      idx = self._5th_rule(j_states)
    elif rule_idx == 5:
      idx = self._6th_rule(j_states)
    else:
      raise ValueError("Wrong mode for composite rule: {}".format(rule_idx))
    return np.eye(output_dim)[idx]

  @staticmethod
  def _1st_rule(j_states):
    """j_states: (OP_i, n_i, D_i, sum(t_ij), now)"""
    tardy_jobs = (j_states[:, 0] < j_states[:, 1]) & (j_states[:, 2] < j_states[:, 4])
    if True not in tardy_jobs:
      return np.argmin((j_states[:,2] - j_states[:,4]) / (j_states[:,1]-j_states[:,0])).item()
    return np.argmax(j_states[:,3] - j_states[:,2] - np.where(tardy_jobs, 0, np.inf))

  @staticmethod
  def _2nd_rule(j_states):
    """j_states: (OP_i, n_i, D_i, sum(t_ij), now)"""
    tardy_jobs = (j_states[:, 0] < j_states[:, 1]) & (j_states[:, 2] < j_states[:, 4])
    if True not in tardy_jobs:
      return np.argmin((j_states[:,2] - j_states[:,4]) / j_states[:,3]).item()
    return np.argmax(j_states[:,3] - j_states[:,2] - np.where(tardy_jobs, 0, np.inf))

  @staticmethod
  def _3rd_rule(j_states):
    """j_states: (OP_i, n_i, D_i, sum(t_ij), now)"""
    return np.argmax(j_states[:,3] - j_states[:,2])

  @staticmethod
  def _4th_rule(cnt):
    return np.random.choice(cnt, 1).item()

  @staticmethod
  def _5th_rule(j_states):
    """j_states: (OP_i, n_i, D_i, sum(t_ij), now)"""
    eps = 1e-5
    tardy_jobs = (j_states[:, 0] < j_states[:, 1]) & (j_states[:, 2] < j_states[:, 4])
    if True not in tardy_jobs:
      return np.argmin(((j_states[:,0]+eps)/j_states[:,1])*(j_states[:,2] - j_states[:,4])).item()
    x = j_states[:,1] / (j_states[:,0]+eps)
    y = j_states[:,4] + j_states[:,3] - j_states[:,2]
    return np.argmax(x * y - np.where(tardy_jobs, 0, np.inf))

  @staticmethod
  def _6th_rule(j_states):
    """j_states: (OP_i, n_i, D_i, sum(t_ij), now)"""
    return np.argmax(j_states[:,3] - j_states[:,2])