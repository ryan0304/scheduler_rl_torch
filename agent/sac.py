import numpy as np
import torch

from agent.critic import DoubleQCritic
from agent.actor import MultinomialPolicy
from utils import to_np
from .utils import Agent, soft_update_params


class SoftActorCriticAgent(Agent):
  """SAC algorithm with TD error
  [1] https://spinningup.openai.com/en/latest/algorithms/sac.html
  [2] https://github.com/denisyarats/pytorch_sac
  """
  training: bool

  def __init__(self, obs_dim, action_dim, is_continuous_action, action_range, critic_range, device,
               hidden_dim, hidden_depth, actor_temp, discount, init_temperature, alpha_lr,
               alpha_betas, actor_lr, actor_betas, critic_lr, critic_betas, critic_tau,
               critic_target_update_frequency, batch_size, learnable_temperature, epsilon,
               model_path, target_entropy=None):
    super().__init__(obs_dim, action_dim, torch.device(device))

    self.is_continuous_action = is_continuous_action
    self.action_range = action_range
    self.device = torch.device(device)
    self.discount = discount
    self.critic_tau = critic_tau
    self.critic_target_update_frequency = critic_target_update_frequency
    self.batch_size = batch_size
    self.learnable_temperature = learnable_temperature
    self.epsilon = epsilon
    self.model_path = model_path

    self.critic = DoubleQCritic(obs_dim, action_dim, hidden_dim, hidden_depth, critic_range).to(self.device)
    self.critic_target = DoubleQCritic(obs_dim, action_dim, hidden_dim, hidden_depth, critic_range).to(self.device)
    self.critic_target.load_state_dict(self.critic.state_dict())

    self.temp = torch.tensor(actor_temp).to(self.device)
    self.actor = MultinomialPolicy(obs_dim, action_dim, hidden_dim, hidden_depth, self.temp).to(self.device)

    self.log_alpha = torch.tensor(np.log(init_temperature)).to(self.device)
    self.log_alpha.requires_grad = True

    # set target entropy to -|A| or -0.98 * (-log(1/|A|))
    if target_entropy:
      self.target_entropy = target_entropy
    # elif not self.is_continuous_action:
    #   self.target_entropy = 0.98 * np.log(1 / action_dim)
    else:
      self.target_entropy = -action_dim

    # optimizers
    self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                            lr=actor_lr,
                                            betas=actor_betas)

    self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                             lr=critic_lr,
                                             betas=critic_betas)

    self.log_alpha_optimizer = torch.optim.Adam([self.log_alpha],
                                                lr=alpha_lr,
                                                betas=alpha_betas)

    self.train()
    self.critic_target.train()

  def train(self, training=True):
    self.training = training
    self.actor.train(training)
    self.critic.train(training)

  @property
  def alpha(self):
    return self.log_alpha.exp()

  def select_action(self, obs, sample=False):
    obs = torch.FloatTensor(obs).to(self.device)
    obs = obs.unsqueeze(0)
    dist = self.actor(obs)
    if self.is_continuous_action:
      action = dist.sample() if sample else dist.mean
      action = action.clamp(*self.action_range)
    else:
      action = self.actor.hard_sample(dist)
    assert action.ndim == 2 and action.shape[0] == 1
    return to_np(action[0])

  def update_critic(self, obs, action, reward, next_obs, not_done, weight, ret):
    # Calculate target Q
    if self.use_gae:
      target_V1, target_V2 = self.critic_target.calc_value(next_obs)
      target_V = torch.min(target_V1, target_V2)
    else:
      dist = self.actor(next_obs)
      next_action = dist.rsample()
      if self.is_continuous_action:
        log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
      else:
        # for one action
        log_prob = dist.log_prob(next_action)
        log_prob = log_prob.unsqueeze(1)
        # for avg of actions
        # log_prob = -torch.sum(dist.probs * dist.logits, dim=-1, keepdim=True)
      target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
      target_V = torch.min(target_Q1, target_Q2) - self.alpha.detach() * log_prob
    target_Q = reward + (not_done * self.discount * target_V)
    target_Q = target_Q.detach()
    # target_Q = ret

    # Critic loss for Q
    current_Q1, current_Q2 = self.critic(obs, action)
    td_error1 = current_Q1 - target_Q
    td_error2 = current_Q2 - target_Q
    critic_loss = (td_error1.pow(2) * weight).mean() + (td_error2.pow(2) * weight).mean()

    # Optimize the critic
    self.critic_optimizer.zero_grad()
    critic_loss.backward()
    self.critic_optimizer.step()

    # TD error
    TD_error = 0.5 * (torch.abs(td_error1) + torch.abs(td_error2)) + self.epsilon

    # Q values
    Q_value = 0.5 * (current_Q1.mean() + current_Q2.mean())

    return to_np(critic_loss), to_np(TD_error), to_np(Q_value)

  def update_actor_and_alpha(self, obs, weight):
    # Get log_prob
    dist = self.actor(obs)
    action = dist.rsample()
    if self.is_continuous_action:
      log_prob = dist.log_prob(action).sum(-1, keepdim=True)
    else:
      # for one action
      log_prob = dist.log_prob(action)
      log_prob = log_prob.unsqueeze(1)
      # for avg of actions
      # log_prob = -torch.sum(dist.probs * dist.logits, dim=-1, keepdim=True)

    # Actor loss
    actor_Q1, actor_Q2 = self.critic(obs, action)
    actor_Q = torch.min(actor_Q1, actor_Q2)
    if self.use_gae:
      actor_V1, actor_V2 = self.critic.calc_value(obs)
      actor_V = torch.min(actor_V1, actor_V2)
      actor_Q = actor_Q - actor_V
    actor_loss = ((self.alpha.detach() * log_prob - actor_Q) * weight).mean()

    # Optimize the actor
    self.actor_optimizer.zero_grad()
    actor_loss.backward()
    self.actor_optimizer.step()

    # Optimize alpha
    alpha_loss = torch.tensor([0])
    if self.learnable_temperature:
      alpha_loss = ((self.alpha * (-log_prob - self.target_entropy).detach()) * weight).mean()
      self.log_alpha_optimizer.zero_grad()
      alpha_loss.backward()
      self.log_alpha_optimizer.step()

    return to_np(actor_loss), to_np(self.alpha), to_np(alpha_loss)

  def update_value(self, obs, weight, ret):
    dist = self.actor(obs)
    action = dist.rsample()
    if self.is_continuous_action:
      log_prob = dist.log_prob(action).sum(-1, keepdim=True)
    else:
      # for one action
      log_prob = dist.log_prob(action)
      log_prob = log_prob.unsqueeze(1)
    target_V = ret - (self.alpha * log_prob).detach()

    actor_V1, actor_V2 = self.critic.calc_value(obs)
    v_error1 = actor_V1 - target_V
    v_error2 = actor_V2 - target_V
    value_loss = (v_error1.pow(2) * weight).mean() + (v_error2.pow(2) * weight).mean()

    # Optimize the critic
    self.critic_optimizer.zero_grad()
    value_loss.backward()
    self.critic_optimizer.step()

    return to_np(value_loss)

  def update(self, replay_buffer, step, n_epi, k, n_step):
    obs, action, reward, next_obs, not_done, weight, ret, idxs = replay_buffer.sample(
      self.batch_size, n_epi, k, n_step)

    critic_loss, prios, q_value = self.update_critic(
      obs, action, reward, next_obs, not_done, weight, ret)

    actor_loss, alpha, alpha_loss = self.update_actor_and_alpha(obs, weight)

    value_loss = 0
    if self.use_gae:
      value_loss = self.update_value(obs, weight, ret)

    if step % self.critic_target_update_frequency == 0:
      soft_update_params(self.critic, self.critic_target, self.critic_tau)

    replay_buffer.update_priority(idxs, prios)

    return critic_loss, actor_loss, alpha_loss, value_loss, q_value, alpha

  def save_model(self, iteration, model_path=None):
    if not model_path:
      model_path = self.model_path
    torch.save({
      "critic_state_dict": self.critic.state_dict(),
      "actor_state_dict": self.actor.state_dict(),
      "log_alpha": self.log_alpha,
      "optimizer_critic_state_dict": self.critic_optimizer.state_dict(),
      "optimizer_actor_state_dict": self.actor_optimizer.state_dict(),
      "optimizer_alpha_state_dict": self.log_alpha_optimizer.state_dict(),
      "iteration": iteration
    }, model_path)
    self.int_scaler.save()
    self.r_scaler.save()

  def load_model(self, model_path=None):
    if not model_path:
      model_path = self.model_path
    checkpoint = torch.load(model_path)
    self.critic.load_state_dict(checkpoint['critic_state_dict'])
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.actor.load_state_dict((checkpoint['actor_state_dict']))
    self.log_alpha = checkpoint["log_alpha"]
    self.critic_optimizer.load_state_dict(checkpoint['optimizer_critic_state_dict'])
    self.actor_optimizer.load_state_dict(checkpoint['optimizer_actor_state_dict'])
    self.log_alpha_optimizer.load_state_dict(checkpoint['optimizer_alpha_state_dict'])
    self.int_scaler.load()
    self.r_scaler.load()
    print('[ load model ] iteration=%d' % checkpoint["iteration"])