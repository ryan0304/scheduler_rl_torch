import torch
import copy
import os
import numpy as np

from agent.critic import QCritic
from agent.composite_rules import CompositeRules
from utils import to_np
from .utils import Agent, soft_update_params


class DQNAgent(Agent):
  """Deep Q Network
  [1] https://spinningup.openai.com/en/latest/spinningup/keypapers.html?highlight=dqn#a-deep-q-learning
  """
  training: bool

  def __init__(self, obs_dim, action_dim, len_dispatching_state, len_compare_state,
               device, d_model, hidden_depth, batch_size, discount, tau, eps, eps_decay,
               critic_range, critic_lr, critic_betas, epsilon, model_path):
    super(DQNAgent, self).__init__(obs_dim, action_dim, torch.device(device))

    self.obs_dim = obs_dim
    self.action_dim = action_dim
    self.len_dispatching_state = len_dispatching_state
    self.len_compare_state = len_compare_state
    self.device = torch.device(device)
    self.d_model = d_model
    self.hidden_depth = hidden_depth
    self.batch_size = batch_size
    self.discount = discount
    self.tau = tau
    self.eps = eps
    self.eps_decay = eps_decay
    self.critic_range = critic_range
    self.critic_lr = critic_lr
    self.critic_betas = critic_betas
    self.epsilon = epsilon
    self.model_path = model_path

    self.step = 0

    wroute = '/'.join(model_path.split('/')[:-1])
    if not os.path.isdir(wroute):
      os.mkdir(wroute)

    self.rules = CompositeRules(len_dispatching_state=len_dispatching_state)

    self.critic = QCritic(len_compare_state, action_dim, d_model, hidden_depth, critic_range).to(self.device)
    self.critic_target = copy.deepcopy(self.critic)
    self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                             lr=critic_lr,
                                             betas=critic_betas)
    self.train()
    self.critic_target.train()

  def train(self, training=True):
    self.training = training
    self.critic.train(training)

  def select_action(self, obs, sample=True):
    assert np.array(obs).ndim == 1

    eps_ = self.eps[1] + (self.eps[0] - self.eps[1]) * np.exp(-1. * self.step / self.eps_decay)

    if (sample is True) and (np.random.random() < eps_):
      idx = 3   # 4th rule of composite rules is random selection
    else:
      critic_state = torch.FloatTensor(obs[:self.len_compare_state]).to(self.device)
      critic_state = critic_state.unsqueeze(0)
      logits = to_np(self.critic(critic_state))
      idx = np.argmax(logits)

    dispatching_state = obs[self.len_compare_state:]
    action = self.rules.select_action(dispatching_state, idx)

    return action

  def update_critic(self, obs, reward, next_obs, not_done, weight):
    # Erase dispatching state
    obs = obs[:, :self.len_compare_state]
    next_obs = next_obs[:, :self.len_compare_state]

    # Compute the target Q value
    with torch.no_grad():
      target_V = self.critic_target(next_obs)
    target_Q = reward + not_done * self.discount * target_V

    # Get current Q estimates
    current_Q = self.critic(obs)
    td_error = current_Q - target_Q

    # Calculate loss
    critic_loss = (td_error.pow(2) * weight).mean()

    # Optimize the critic
    self.critic_optimizer.zero_grad()
    critic_loss.backward()
    self.critic_optimizer.step()

    # TD error
    TD_error = td_error.abs().mean(axis=1) + self.epsilon

    # Q values
    Q_value = current_Q.max()

    return to_np(critic_loss), to_np(TD_error), to_np(Q_value)

  def update(self, replay_buffer, iteration, n_epi, k, n_step):
    obs, action, reward, next_obs, not_done, weight, ret, idxs = replay_buffer.sample(
      self.batch_size, n_epi, k, n_step)

    critic_loss, prios, q_value = self.update_critic(
      obs, reward, next_obs, not_done, weight)

    self.step += 1
    soft_update_params(self.critic, self.critic_target, self.tau)
    replay_buffer.update_priority(idxs, prios)

    int_loss = np.nan
    if self.curiosity_mode == "rnd":
      int_loss = self.curiosity.update(next_obs)
    elif self.curiosity_mode == "ride":
      int_loss = self.curiosity.update(obs, action, next_obs)

    return critic_loss, int_loss, q_value

  def save_model(self, iteration, model_path=None):
    if not model_path:
      model_path = self.model_path
    torch.save({
      "critic_state_dict": self.critic.state_dict(),
      "optimizer_critic_state_dict": self.critic_optimizer.state_dict(),
      "iteration": iteration
    }, model_path)
    self.int_scaler.save()
    self.r_scaler.save()

  def load_model(self, model_path=None):
    if not model_path:
      model_path = self.model_path
    checkpoint = torch.load(model_path, map_location=self.device)
    self.critic.load_state_dict(checkpoint['critic_state_dict'])
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.critic_optimizer.load_state_dict(checkpoint['optimizer_critic_state_dict'])
    self.int_scaler.load()
    self.r_scaler.load()
    print('[ load model ] iteration=%d' % checkpoint["iteration"])