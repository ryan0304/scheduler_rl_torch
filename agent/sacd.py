import numpy as np
import torch

from agent.critic import TwinnedQCritic
from agent.actor import CategoricalPolicy
from utils import to_np
from .utils import Agent, soft_update_params


class SoftActorCriticDiscreteAgent(Agent):
  """SAC-discrete algorithm with TD error
  [1] https://github.com/ku2482/sac-discrete.pytorch
  """
  training: bool

  def __init__(self, obs_dim, action_dim, critic_range, device, hidden_dim, hidden_depth,
               discount, init_temperature, alpha_lr, alpha_betas, actor_lr, actor_betas,
               critic_lr, critic_betas, critic_tau, critic_target_update_frequency,
               batch_size, learnable_temperature, epsilon, model_path, target_entropy=None):
    super().__init__(obs_dim, action_dim, torch.device(device))

    self.device = torch.device(device)
    self.discount = discount
    self.critic_tau = critic_tau
    self.critic_target_update_frequency = critic_target_update_frequency
    self.batch_size = batch_size
    self.learnable_temperature = learnable_temperature
    self.epsilon = epsilon
    self.model_path = model_path

    self.critic = TwinnedQCritic(obs_dim, action_dim, hidden_dim, hidden_depth, critic_range).to(self.device)
    self.critic_target = TwinnedQCritic(obs_dim, action_dim, hidden_dim, hidden_depth, critic_range).to(self.device)
    self.critic_target.load_state_dict(self.critic.state_dict())

    self.actor = CategoricalPolicy(obs_dim, action_dim, hidden_dim, hidden_depth).to(self.device)

    self.log_alpha = torch.tensor(np.log(init_temperature)).to(self.device)
    self.log_alpha.requires_grad = True

    # set target entropy to -0.98 * (-log(1/|A|))
    self.target_entropy = target_entropy if target_entropy else 0.98 * np.log(1 / action_dim)

    # optimizers
    self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                            lr=actor_lr,
                                            betas=actor_betas)

    self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                             lr=critic_lr,
                                             betas=critic_betas)

    self.log_alpha_optimizer = torch.optim.Adam([self.log_alpha],
                                                lr=alpha_lr,
                                                betas=alpha_betas)

    self.train()
    self.critic_target.train()

  def train(self, training=True):
    self.training = training
    self.actor.train(training)
    self.critic.train(training)

  @property
  def alpha(self):
    return self.log_alpha.exp()

  def select_action(self, obs, sample=False):
    obs = torch.FloatTensor(obs).to(self.device)
    obs = obs.unsqueeze(0)
    dist = self.actor(obs)
    action = self.actor.hard_sample(dist)
    assert action.ndim == 2 and action.shape[0] == 1
    return to_np(action[0])

  def update_critic(self, obs, action, reward, next_obs, not_done, weight, ret):
    # Calculate target Q
    if self.use_gae:
      target_Q = ret
    else:
      with torch.no_grad():
        _, action_probs, log_action_probs = self.actor.sample(next_obs)
        target_Q1, target_Q2 = self.critic_target(next_obs)
        target_V = (action_probs * (
          torch.min(target_Q1, target_Q2) - self.alpha * log_action_probs
        )).sum(dim=-1, keepdim=True)

      target_Q = reward + (not_done * self.discount * target_V)

    # Critic loss for Q
    current_Q1, current_Q2 = self.critic(obs)
    action_index = torch.argmax(action, dim=-1, keepdim=True)
    td_error1 = current_Q1.gather(1, action_index.long()) - target_Q
    td_error2 = current_Q2.gather(1, action_index.long()) - target_Q
    critic_loss = (td_error1.pow(2) * weight).mean() + (td_error2.pow(2) * weight).mean()

    # Optimize the critic
    self.critic_optimizer.zero_grad()
    critic_loss.backward()
    self.critic_optimizer.step()

    # TD error
    TD_error = (0.5 * (torch.abs(td_error1) + torch.abs(td_error2)) + self.epsilon).mean(dim=-1)

    # Q values
    Q_value = 0.5 * (current_Q1.mean() + current_Q2.mean())

    return to_np(critic_loss), to_np(TD_error), to_np(Q_value)

  def update_actor_and_alpha(self, obs, weight):
    # Get log_prob
    _, action_probs, log_action_probs = self.actor.sample(obs)

    with torch.no_grad():
      q1, q2 = self.critic(obs)
      q = torch.min(q1, q2)

    entropies = -torch.sum(action_probs * log_action_probs, dim=-1, keepdim=True)

    q = torch.sum(q * action_probs, dim=-1, keepdim=True)

    actor_loss = ((-q - self.alpha.detach() * entropies) * weight).mean()

    # Optimize the actor
    self.actor_optimizer.zero_grad()
    actor_loss.backward()
    self.actor_optimizer.step()

    # Optimize alpha
    alpha_loss = torch.tensor([0])
    if self.learnable_temperature:
      alpha_loss = ((self.alpha * (entropies - self.target_entropy).detach()) * weight).mean()
      self.log_alpha_optimizer.zero_grad()
      alpha_loss.backward()
      self.log_alpha_optimizer.step()

    return to_np(actor_loss), to_np(self.alpha), to_np(alpha_loss)

  def update(self, replay_buffer, step, n_epi, k, n_step):
    obs, action, reward, next_obs, not_done, weight, ret, idxs = replay_buffer.sample(
      self.batch_size, n_epi, k, n_step)

    critic_loss, prios, q_value = self.update_critic(
      obs, action, reward, next_obs, not_done, weight, ret)

    actor_loss, alpha, alpha_loss = self.update_actor_and_alpha(obs, weight)

    value_loss = 0

    if step % self.critic_target_update_frequency == 0:
      soft_update_params(self.critic, self.critic_target, self.critic_tau)

    replay_buffer.update_priority(idxs, prios)

    return critic_loss, actor_loss, alpha_loss, value_loss, q_value, alpha

  def save_model(self, iteration, model_path=None):
    if not model_path:
      model_path = self.model_path
    torch.save({
      "critic_state_dict": self.critic.state_dict(),
      "actor_state_dict": self.actor.state_dict(),
      "log_alpha": self.log_alpha,
      "optimizer_critic_state_dict": self.critic_optimizer.state_dict(),
      "optimizer_actor_state_dict": self.actor_optimizer.state_dict(),
      "optimizer_alpha_state_dict": self.log_alpha_optimizer.state_dict(),
      "iteration": iteration
    }, model_path)
    self.int_scaler.save()
    self.r_scaler.save()

  def load_model(self, model_path=None):
    if not model_path:
      model_path = self.model_path
    checkpoint = torch.load(model_path)
    self.critic.load_state_dict(checkpoint['critic_state_dict'])
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.actor.load_state_dict((checkpoint['actor_state_dict']))
    self.log_alpha = checkpoint["log_alpha"]
    self.critic_optimizer.load_state_dict(checkpoint['optimizer_critic_state_dict'])
    self.actor_optimizer.load_state_dict(checkpoint['optimizer_actor_state_dict'])
    self.log_alpha_optimizer.load_state_dict(checkpoint['optimizer_alpha_state_dict'])
    self.int_scaler.load()
    self.r_scaler.load()
    print('[ load model ] iteration=%d' % checkpoint["iteration"])