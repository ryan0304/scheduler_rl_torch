import os
import torch
import copy
import numpy as np

from models import ETCEEncoder
from agent.critic import DoubleQCriticTransformer
from agent.actor import MultinomialPolicyTransformer
from utils import to_np
from .utils import Agent, soft_update_params, copy_params


class TD3AgentTransformer(Agent):
  """Twin Delayed Deep Deterministic Policy Gradient with TD error
  [1] https://spinningup.openai.com/en/latest/algorithms/td3.html
  [2] https://github.com/sfujim/TD3/blob/master/TD3.py
  """

  def __init__(self, action_dim, critic_range, device,
               len_global_state, len_j_state, len_unit_state, num_embeddings,
               d_model, num_heads, dff, num_layers, dropout,
               actor_temp, discount, actor_lr, actor_betas, critic_lr, critic_betas,
               tau, policy_update_frequency, batch_size, epsilon, model_path):
    super(TD3AgentTransformer, self).__init__()

    self.device = torch.device(device)
    self.discount = discount
    self.tau = tau
    self.policy_update_frequency = policy_update_frequency
    self.batch_size = batch_size
    self.epsilon = epsilon
    self.model_path = model_path

    model_dir = '/'.join(model_path.split('/')[:-1])
    if not os.path.exists(model_dir):
      os.makedirs(model_dir)

    self.etce_encoder = etce_encoder = ETCEEncoder(
      len_global_state, len_j_state, len_unit_state, num_embeddings,
      d_model, num_heads, dff, num_layers, dropout)

    self.critic = DoubleQCriticTransformer(etce_encoder, action_dim, d_model, critic_range).to(self.device)
    self.critic_target = copy.deepcopy(self.critic)

    self.temp = torch.tensor(actor_temp).to(self.device)
    self.actor = MultinomialPolicyTransformer(
      etce_encoder, len_unit_state, action_dim, len_j_state, d_model, self.temp).to(self.device)
    self.actor_target = copy.deepcopy(self.actor)

    # optimizers
    self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                            lr=actor_lr,
                                            betas=actor_betas)

    self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                             lr=critic_lr,
                                             betas=critic_betas)

    self.train(True)

  def train(self, training=True):
    self.actor.train(training)
    self.critic.train(training)
    self.actor_target.train()
    self.critic_target.train()

  def select_action(self, obs, sample=False):
    obs = torch.FloatTensor(obs).to(self.device)
    obs = obs.unsqueeze(0)
    dist = self.actor(obs)
    action = self.actor.hard_sample(dist)
    assert action.ndim == 2 and action.shape[0] == 1
    return to_np(action[0])

  def update_critic(self, obs, action, reward, next_obs, not_done, weight):
    with torch.no_grad():
      # Rsample next action
      dist = self.actor_target(next_obs)
      next_action = dist.rsample()

      # Compute the target Q value
      target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
      target_V = torch.min(target_Q1, target_Q2)
      target_Q = reward + not_done * self.discount * target_V

    # Get current Q estimates
    current_Q1, current_Q2 = self.critic(obs, action)
    td_error1 = current_Q1 - target_Q
    td_error2 = current_Q2 - target_Q
    critic_loss = (td_error1.pow(2) * weight).mean() + (td_error2.pow(2) * weight).mean()

    # Optimize the critic
    self.critic_optimizer.zero_grad()
    critic_loss.backward()
    self.critic_optimizer.step()

    # TD error
    TD_error = 0.5 * (torch.abs(td_error1) + torch.abs(td_error2)) + self.epsilon

    # Q values
    Q_value = 0.5 * (current_Q1.mean() + current_Q2.mean())

    return to_np(critic_loss), to_np(TD_error), to_np(Q_value)

  def update_actor(self, obs, weight):
    # select action
    dist = self.actor(obs)
    action = dist.probs

    # Compute actor loss
    actor_loss = -(self.critic.compute_Q1(obs, action) * weight).mean()

    # optimize the actor
    self.actor_optimizer.zero_grad()
    actor_loss.backward()
    self.actor_optimizer.step()

    return to_np(actor_loss)

  def update(self, replay_buffer, ride, iteration, n_epi, k, n_step):
    obs, action, reward, next_obs, not_done, weight, idxs = replay_buffer.sample(
      self.batch_size, n_epi, k, n_step)

    critic_loss, prios, q_value = self.update_critic(obs, action, reward, next_obs, not_done, weight)

    actor_loss = np.nan
    if iteration % self.policy_update_frequency == 0:
      actor_loss = self.update_actor(obs, weight)

      # Update the frozen target models
      soft_update_params(self.critic, self.critic_target, self.tau)
      soft_update_params(self.actor, self.actor_target, self.tau)

    replay_buffer.update_priority(idxs, prios)
    ride_loss = ride.update(obs, action, next_obs)

    return critic_loss, actor_loss, ride_loss, q_value

  def save_model(self, iteration, model_path=None):
    if model_path is None:
      model_path = self.model_path
    torch.save({
      "critic_state_dict": self.critic.state_dict(),
      "actor_state_dict": self.actor.state_dict(),
      "optimizer_critic_state_dict": self.critic_optimizer.state_dict(),
      "optimizer_actor_state_dict": self.actor_optimizer.state_dict(),
      "iteration": iteration
    }, model_path)

  def load_model(self, model_path=None):
    if not model_path:
      model_path = self.model_path
    checkpoint = torch.load(model_path)
    self.critic.load_state_dict(checkpoint['critic_state_dict'])
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.actor.load_state_dict((checkpoint['actor_state_dict']))
    self.actor_target.load_state_dict(self.actor.state_dict())
    self.critic_optimizer.load_state_dict(checkpoint['optimizer_critic_state_dict'])
    self.actor_optimizer.load_state_dict(checkpoint['optimizer_actor_state_dict'])
    print('[ load model ] iteration=%d' % checkpoint["iteration"])