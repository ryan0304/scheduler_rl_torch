import abc
import torch
import random
import numpy as np
from copy import copy
import torch.nn.functional as F

from config import Configs
from utils import RewardScaler, to_np
from .rnd import RandomNetworkDistillation
from .ride import RewardImpactDrivenExploration


class Agent(object):
  def __init__(self, obs_dim, action_dim, device):
    super(Agent, self).__init__()
    self.environment = Configs.ExperimentConfig.environment
    self.is_credit_assignment = Configs.ExperimentConfig.is_credit_assignment
    self.is_scaling_reward = Configs.ExperimentConfig.is_scaling_reward
    self.int_reward_clipping = Configs.ExperimentConfig.int_reward_clipping
    self.curiosity_mode = Configs.ExperimentConfig.curiosity_mode
    self.use_gae = Configs.ExperimentConfig.use_gae
    self.clipping_gradient = Configs.ExperimentConfig.clipping_gradient
    self.epi_without_scaling = Configs.ExperimentConfig.epi_without_scaling
    self.is_state_embedding = Configs.ExperimentConfig.is_state_embedding

    self.gamma = Configs.GAEConfig.gamma
    self.lambda_ = Configs.GAEConfig.lambda_

    self.device = device

    # self.alpha : cannot set value
    self.critic = None
    self.actor = None
    self.actor_target = None
    self.actor_optimizer = None
    self.batch_size = None
    self.min_nonpad_job = 1

    # scaler
    self.int_scaler = RewardScaler(scaler_path=Configs.ScalerConfig.intrinsic_scaler_path)
    self.r_scaler = RewardScaler(scaler_path=Configs.ScalerConfig.reward_scaler_path)

    # ride or rnd
    self.curiosity = None
    if self.curiosity_mode == "rnd":
      self.curiosity = RandomNetworkDistillation(input_dim=obs_dim,
                                                 device=device,
                                                 **Configs.RNDConfig.to_dict())
    elif self.curiosity_mode == "ride":
      self.curiosity = RewardImpactDrivenExploration(input_dim=obs_dim,
                                                     action_dim=action_dim,
                                                     device=device,
                                                     **Configs.RIDEConfig.to_dict())

  def reset(self):
    """For state-full agents this function performs resetting at the beginning of each episode."""
    pass

  @abc.abstractmethod
  def train(self, training=True):
    """Sets the agent in either training or evaluation mode."""

  @abc.abstractmethod
  def update(self, replay_buffer, step, n_epi, k, n_step):
    """Main function of the agent that performs learning."""

  @abc.abstractmethod
  def select_action(self, obs, sample=False):
    """Issues an action given an observation.
    sample: for SAC not for TD3"""

  @abc.abstractmethod
  def save_model(self, iteration):
    """Save deep models."""

  @abc.abstractmethod
  def load_model(self):
    """Load deep models."""

  def handle_replay_buffer(self, tmp_buffer, n_epi,
                           critic=None, final_reward=None, actor=None):
    sum_reward, sum_intrinsic, values = 0, 0, []
    for i, (state, action, reward, next_state, done) in enumerate(tmp_buffer):
      # Delayed reward
      if final_reward:
        reward = final_reward

      # Scaling reward
      if self.is_scaling_reward:
        reward = self.get_scaled_reward(reward, n_epi)

      sum_reward += reward

      # Curiosity
      if self.curiosity_mode != "no_curiosity":
        int_reward = self.get_intrinsic_reward(state, next_state, n_epi)
        reward += int_reward
        sum_intrinsic += int_reward

      # apply changed reward
      tmp_buffer[i] = (state, action, reward, next_state, done)

      if self.use_gae:
        value = self.get_gae_value(state, actor, critic)
        values.append(value)

    # Returns for GAE
    if self.use_gae:
      tmp_buffer = self.apply_gae(tmp_buffer, values)

    return tmp_buffer, sum_reward, sum_intrinsic

  def handle_jobshop_buffer(self, tmp_buffer, final_reward, env, n_epi):
    """no scaling"""
    sum_reward, sum_intrinsic = 0, 0
    total_machine_buffer = []
    for list_of_tuple in tmp_buffer.values():
      seq_len = len(list_of_tuple) - 1
      if seq_len < 1:
        continue

      one_machine_buffer, values = [], []
      for i in range(seq_len):
        state, action, reward, now = list_of_tuple[i]
        if env.count_nonpad_job(state) < self.min_nonpad_job:
          continue
        next_state = list_of_tuple[i + 1][0]
        if self.is_credit_assignment and final_reward:
          importance = [1, state[0] * state[env.len_global_state - 1]][0]  # finish_ratio * bottleneck_index
          reward = reward*0.005 + final_reward * importance
          # reward = final_reward
        sum_reward += reward

        # Curiosity
        if self.curiosity_mode != "no_curiosity":
          int_reward = self.get_intrinsic_reward(state, next_state, n_epi)
          reward += int_reward
          sum_intrinsic += int_reward

        done = 0 if i < seq_len - 1 else 1
        replay_memory_set = [now, state, action, reward, next_state, done]
        one_machine_buffer.append(replay_memory_set)

        if self.use_gae:
          value = self.get_gae_value(state, self.actor, self.critic)
          values.append(value)

      # Returns for GAE
      if self.use_gae and one_machine_buffer:
        one_machine_buffer = self.apply_gae(one_machine_buffer, values)

      total_machine_buffer.extend(one_machine_buffer)

    # Sort by time and eliminate time
    total_machine_buffer = [x[1:] for x in sorted(total_machine_buffer)]

    return total_machine_buffer, sum_reward, sum_intrinsic

  def handle_compare_buffer(self, tmp_buffer, env):
    """no scaling"""
    sum_reward = 0
    total_machine_buffer = []
    for list_of_tuple in tmp_buffer.values():
      seq_len = len(list_of_tuple) - 1
      if seq_len < 1:
        continue

      one_machine_buffer, values = [], []
      for i in range(seq_len):
        state, action, reward, now = list_of_tuple[i]
        if env.count_nonpad_job(state) < self.min_nonpad_job:
          continue
        next_state = list_of_tuple[i + 1][0]
        reward = self.get_compare_reward(state, next_state)
        done = 0 if i < seq_len - 1 else 1

        sum_reward += reward
        replay_memory_set = [now, state, action, reward, next_state, done]
        one_machine_buffer.append(replay_memory_set)

      total_machine_buffer.extend(one_machine_buffer)

    # Sort by time and eliminate time
    total_machine_buffer = [x[1:] for x in sorted(total_machine_buffer)]

    return total_machine_buffer, sum_reward

  def get_scaled_reward(self, reward, n_epi):
    clip_range = 3
    is_push = False if n_epi < self.epi_without_scaling else True

    reward = self.r_scaler(reward, is_push)
    reward = np.clip(reward, -clip_range, clip_range)
    return reward

  def get_compare_reward(self, state, next_state):
    U_ave, Tard_e, Tard_a = state[0], state[5], state[6]
    U_ave_p, Tard_e_p, Tard_a_p = next_state[0], next_state[5], next_state[6]

    if Tard_a_p < Tard_a:
      return 1
    elif Tard_a_p > Tard_a:
      return -1
    elif Tard_e_p < Tard_e:
      return 1
    elif Tard_e_p > Tard_e:
      return -1
    elif U_ave_p > U_ave:
      return 1
    elif U_ave_p > U_ave * 0.95:
      return 0
    return -1


  def get_intrinsic_reward(self, state, next_state, n_epi):
    is_push = False if n_epi < self.epi_without_scaling else True

    if self.curiosity_mode == "rnd":
      int_reward = self.curiosity.get_int_reward(next_state)
    elif self.curiosity_mode == "ride":
      int_reward = self.curiosity.get_int_reward(state, next_state)
    else:
      raise NotImplementedError("Available curiosity_mode: 'rnd', 'ride'")

    if self.int_reward_clipping is not None:
      value = abs(self.int_reward_clipping)
      int_reward = self.int_scaler(int_reward, is_push)
      int_reward = np.clip(int_reward, -value, value) - value

    return int_reward

  def get_gae_value(self, state, actor, critic):
    # state to tensor
    state_tensor = torch.as_tensor(state, device=self.device).float().reshape(1, -1)

    # value prediction
    with torch.no_grad():
      if Configs.ExperimentConfig.agent_mode == Configs.RLAgent.SACD:
        # calculate expectation of state-action values
        _, action_probs, log_action_probs = actor.sample(state_tensor)
        target_Q1, target_Q2 = critic(state_tensor)
        value = (action_probs * (
            torch.min(target_Q1, target_Q2) - self.alpha * log_action_probs
        )).sum(dim=-1, keepdim=True)
        value = to_np(value).item()
      else:
        value1, value2 = critic.calc_value(state_tensor)
        value = min(to_np(value1).item(), to_np(value2).item())

    return value

  def apply_gae(self, tmp_buffer, values):
    """
    tmp_buffer includes:
    (time_stamp), state, action, reward, next_state, done
    """
    aList = copy(tmp_buffer)
    gae = 0
    values.append(0.)
    is_time_included = len(aList[0]) == 6
    for i in reversed(range(len(aList))):
      if is_time_included:
        time_stamp, state, action, reward, next_state, done = aList[i]
      else:
        state, action, reward, next_state, done = aList[i]
        time_stamp = None
      delta = reward + self.gamma * values[i + 1] - values[i]
      gae = delta + self.gamma * self.lambda_ * gae
      ret = gae + values[i]
      if is_time_included:
        aList[i] = (time_stamp, state, action, reward, next_state, done, ret)
      else:
        aList[i] = (state, action, reward, next_state, done, ret)

    return aList

  def pre_update_actor(self, replay_buffer, epoch):
    losses = []
    for _ in range(epoch):
      # construct train dataset
      random.shuffle(replay_buffer)
      chunks = [replay_buffer[i*self.batch_size:(i+1)*self.batch_size]
                for i in range(int(len(replay_buffer)/self.batch_size))]

      # update actor
      for chunk in chunks:
        obses = torch.as_tensor([x[0] for x in chunk], device=self.device).float()
        actions = torch.as_tensor([np.argmax(x[1]) for x in chunk], device=self.device)

        self.actor(obses)
        loss = F.cross_entropy(self.actor.outputs['logits'], actions)

        self.actor_optimizer.zero_grad()
        loss.backward()
        self.actor_optimizer.step()

        losses.append(to_np(loss))

      # update target
      if self.actor_target:
        copy_params(self.actor, self.actor_target)

    return np.mean(losses).item()


def soft_update_params(net, target_net, tau):
  for param, target_param in zip(net.parameters(), target_net.parameters()):
    target_param.data.copy_(tau * param.data +
                            (1 - tau) * target_param.data)


def copy_params(net , target_net):
  for param, target_param in zip(net.parameters(), target_net.parameters()):
    target_param.data.copy_(param.data)
