import torch
import torch.nn as nn
import torch.nn.functional as F
from utils import to_np


class EmbeddingNet(nn.Module):

  def __init__(self, input_dim, d_model):
    super(EmbeddingNet, self).__init__()
    self.norm = nn.LayerNorm(input_dim)
    self.dense1 = nn.Linear(input_dim, d_model)
    self.dense2 = nn.Linear(d_model, d_model)
    self.dense3 = nn.Linear(d_model, d_model)

    # initialization
    for p in self.modules():
      if isinstance(p, nn.Linear):
        nn.init.xavier_uniform_(p.weight)
        p.bias.data.zero_()

  def forward(self, state):
    state = self.norm(state)              # (B, input_dim)
    output = F.elu(self.dense1(state))    # (B, d_model)
    output = F.elu(self.dense2(output))   # (B, d_model)
    output = self.dense3(output)          # (B, d_model)
    return output


class ForwardNet(nn.Module):

  def __init__(self, action_dim, d_model):
    super(ForwardNet, self).__init__()
    self.embedding = nn.Linear(action_dim, d_model)
    self.dense1 = nn.Linear(d_model * 2, d_model)
    self.dense2 = nn.Linear(d_model, d_model)

    # initialization
    for p in self.modules():
      if isinstance(p, nn.Linear):
        nn.init.xavier_uniform_(p.weight)
        p.bias.data.zero_()

  def forward(self, state_embedding, action):
    act = self.embedding(action)    # (B, d_model)
    output = torch.cat((state_embedding, act), dim=1)     # (B, d_model*2)
    output = F.elu(self.dense1(output))                  # (B, d_model)
    output = self.dense2(output)                          # (B, d_model)

    return output


class InverseNet(nn.Module):

  def __init__(self, action_dim, d_model):
    super(InverseNet, self).__init__()
    self.dense1 = nn.Linear(d_model * 2, d_model)
    self.dense2 = nn.Linear(d_model, action_dim)

    # initialization
    for p in self.modules():
      if isinstance(p, nn.Linear):
        nn.init.xavier_uniform_(p.weight)
        p.bias.data.zero_()

  def forward(self, state_embedding, next_state_embedding):
    logits = torch.cat((state_embedding, next_state_embedding), dim=1)    # (B, d_model*2)
    logits = F.elu(self.dense1(logits))                                   # (B, d_model)
    logits = self.dense2(logits)                                          # (B, d_model)

    return logits


class RewardImpactDrivenExploration(nn.Module):
  def __init__(self, device, input_dim, action_dim, ride_lr, d_model):
    super(RewardImpactDrivenExploration, self).__init__()
    self.device = device

    self.embedding = EmbeddingNet(input_dim, d_model).to(self.device)
    self.forward_dynamics = ForwardNet(action_dim, d_model).to(self.device)
    self.inverse_dynamics = InverseNet(action_dim, d_model).to(self.device)

    self.embedding_optimizer = torch.optim.Adam(self.embedding.parameters(), lr=ride_lr)
    self.forward_optimizer = torch.optim.Adam(self.forward_dynamics.parameters(), lr=ride_lr)
    self.inverse_optimizer = torch.optim.Adam(self.inverse_dynamics.parameters(), lr=ride_lr)

    self.embedding.train(True)
    self.forward_dynamics.train(True)
    self.inverse_dynamics.train(True)

  def get_int_reward(self, obs, next_obs):
    # convert state from list to tensor
    obs = torch.as_tensor(obs, device=self.device).float().reshape(1, -1)
    next_obs = torch.as_tensor(next_obs, device=self.device).float().reshape(1, -1)

    # embed
    state_embedding = self.embedding(obs)
    next_state_embedding = self.embedding(next_obs)

    # get reward
    int_reward = torch.norm(next_state_embedding - state_embedding, dim=-1, p=2)

    return int_reward.mean().tolist()

  def update(self, obs, action, next_obs):
    # embed
    state_embedding = self.embedding(obs)
    next_state_embedding = self.embedding(next_obs)

    # forward & inverse
    next_state_prediction = self.forward_dynamics(state_embedding, action)
    action_logit_prediction = self.inverse_dynamics(state_embedding, next_state_embedding)

    # loss
    forward_loss = torch.norm(next_state_prediction - next_state_embedding, dim=-1, p=2)
    forward_loss = forward_loss.mean()
    inverse_loss = F.nll_loss(
      F.log_softmax(action_logit_prediction, dim=-1),
      target=torch.argmax(action, dim=-1),
      reduction='none')
    inverse_loss = inverse_loss.mean()

    total_loss = forward_loss + inverse_loss

    # fit
    self.embedding_optimizer.zero_grad()
    self.forward_optimizer.zero_grad()
    self.inverse_optimizer.zero_grad()
    total_loss.backward()
    self.embedding_optimizer.step()
    self.forward_optimizer.step()
    self.inverse_optimizer.step()

    return to_np(total_loss)
