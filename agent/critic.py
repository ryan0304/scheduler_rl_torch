import torch
from torch import nn

from models import mlp, weight_init, EmbeddingCritic, EmbeddingValue, ETCECritic, ETCEValue


class DoubleQCritic(nn.Module):
  """Critic network, employs double Q-learning."""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth, critic_range):
    super().__init__()

    self.Q1 = mlp(obs_dim + action_dim, hidden_dim, 1, hidden_depth)
    self.Q2 = mlp(obs_dim + action_dim, hidden_dim, 1, hidden_depth)
    self.V1 = mlp(obs_dim, hidden_dim, 1, hidden_depth)
    self.V2 = mlp(obs_dim, hidden_dim, 1, hidden_depth)

    self.critic_range = critic_range
    self.do_clamp = not all(x is None for x in critic_range)
    self.outputs = dict()
    self.apply(weight_init)

  def forward(self, obs, action):
    assert obs.size(0) == action.size(0)

    obs_action = torch.cat([obs, action], dim=-1)
    q1 = self.Q1(obs_action)
    q2 = self.Q2(obs_action)
    if self.do_clamp:
      q1 = q1.clamp(*self.critic_range)
      q2 = q2.clamp(*self.critic_range)

    self.outputs['q1'] = q1
    self.outputs['q2'] = q2

    return q1, q2

  def compute_Q1(self, obs, action):
    """Function for TD3"""
    assert obs.size(0) == action.size(0)

    obs_action = torch.cat([obs, action], dim=-1)
    q1 = self.Q1(obs_action)
    if self.do_clamp:
      q1 = q1.clamp(*self.critic_range)
    return q1

  def calc_value(self, obs):

    v1 = self.V1(obs)
    v2 = self.V2(obs)
    if self.do_clamp:
      v1 = v1.clamp(*self.critic_range)
      v2 = v2.clamp(*self.critic_range)

    self.outputs['v1'] = v1
    self.outputs['v2'] = v2

    return v1, v2

  def compute_V1(self, obs):
    v1 = self.V1(obs)
    if self.do_clamp:
      v1 = v1.clamp(*self.critic_range)
    return v1


class DoubleQCriticMaster(nn.Module):
  """Critic network, employs double Q-learning."""
  def __init__(self, is_state_embedding, action_dim, critic_range, **kwargs):
    super().__init__()

    # mode: base / path-env & embedding / scheduler-env & etce
    if not is_state_embedding:
      base_args = ["obs_dim", "hidden_depth", "d_model"]
      if not all([x in kwargs for x in base_args]):
        raise KeyError("Some args are missing!")

      self.Q1 = mlp(kwargs["obs_dim"] + action_dim, kwargs["d_model"], 1, kwargs["hidden_depth"])
      self.Q2 = mlp(kwargs["obs_dim"] + action_dim, kwargs["d_model"], 1, kwargs["hidden_depth"])
      self.V1 = mlp(kwargs["obs_dim"], kwargs["d_model"], 1, kwargs["hidden_depth"])
      self.V2 = mlp(kwargs["obs_dim"], kwargs["d_model"], 1, kwargs["hidden_depth"])

      self.apply(weight_init)

    elif "embedder" in kwargs:
      self.Q1 = EmbeddingCritic(kwargs["embedder"])
      self.Q2 = EmbeddingCritic(kwargs["embedder"])
      self.V1 = EmbeddingValue(kwargs["embedder"])
      self.V2 = EmbeddingValue(kwargs["embedder"])

    elif "etce_encoder" in kwargs:
      etce_args = ["d_model"]
      if not all([x in kwargs for x in etce_args]):
        raise KeyError("Some args are missing!")
      self.Q1 = ETCECritic(kwargs["etce_encoder"], action_dim, kwargs["d_model"])
      self.Q2 = ETCECritic(kwargs["etce_encoder"], action_dim, kwargs["d_model"])
      self.V1 = ETCEValue(kwargs["etce_encoder"], kwargs["d_model"])
      self.V2 = ETCEValue(kwargs["etce_encoder"], kwargs["d_model"])

    else:
      raise NotImplementedError("State embedding is available in two envs: path or jobshop.")

    self.critic_range = critic_range
    self.do_clamp = not all(x is None for x in critic_range)
    self.outputs = dict()

  def forward(self, obs, action):
    assert obs.size(0) == action.size(0)

    obs_action = torch.cat([obs, action], dim=-1)
    q1 = self.Q1(obs_action)
    q2 = self.Q2(obs_action)
    if self.do_clamp:
      q1 = q1.clamp(*self.critic_range)
      q2 = q2.clamp(*self.critic_range)

    self.outputs['q1'] = q1
    self.outputs['q2'] = q2

    return q1, q2

  def compute_Q1(self, obs, action):
    """Function for TD3"""
    assert obs.size(0) == action.size(0)

    obs_action = torch.cat([obs, action], dim=-1)
    q1 = self.Q1(obs_action)
    if self.do_clamp:
      q1 = q1.clamp(*self.critic_range)
    return q1

  def calc_value(self, obs):

    v1 = self.V1(obs)
    v2 = self.V2(obs)
    if self.do_clamp:
      v1 = v1.clamp(*self.critic_range)
      v2 = v2.clamp(*self.critic_range)

    self.outputs['v1'] = v1
    self.outputs['v2'] = v2

    return v1, v2

  def compute_V1(self, obs):
    v1 = self.V1(obs)
    if self.do_clamp:
      v1 = v1.clamp(*self.critic_range)
    return v1

class TwinnedQCritic(nn.Module):
  """Critic network for SACD"""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth, critic_range):
    super().__init__()

    self.V1 = mlp(obs_dim, hidden_dim, action_dim, hidden_depth)
    self.V2 = mlp(obs_dim, hidden_dim, action_dim, hidden_depth)

    self.critic_range = critic_range
    self.do_clamp = not all(x is None for x in critic_range)
    self.apply(weight_init)

  def forward(self, obs):
    q1 = self.V1(obs)
    q2 = self.V2(obs)
    if self.do_clamp:
      q1 = q1.clamp(*self.critic_range)
      q2 = q2.clamp(*self.critic_range)

    return q1, q2


class QCritic(nn.Module):
  """Critic network for DQN"""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth, critic_range):
    super().__init__()

    self.V1 = mlp(obs_dim, hidden_dim, action_dim, hidden_depth)

    self.critic_range = critic_range
    self.do_clamp = not all(x is None for x in critic_range)
    self.apply(weight_init)

  def forward(self, obs):
    q1 = self.V1(obs)
    if self.do_clamp:
      q1 = q1.clamp(*self.critic_range)

    return q1