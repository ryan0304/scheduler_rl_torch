import torch
import math
import numpy as np
from torch import nn
import torch.nn.functional as F
from torch import distributions as pyd

from models import mlp, weight_init, EmbeddingActor, ETCEActor


# noinspection PyUnresolvedReferences
class TanhTransform(pyd.transforms.Transform):
  domain = pyd.constraints.real
  codomain = pyd.constraints.interval(-1.0, 1.0)
  bijective = True
  sign = +1

  def __init__(self, cache_size=1):
    super().__init__(cache_size=cache_size)

  @staticmethod
  def atanh(x):
    return 0.5 * (x.log1p() - (-x).log1p())

  def __eq__(self, other):
    return isinstance(other, TanhTransform)

  def _call(self, x):
    return x.tanh()

  def _inverse(self, y):
    # We do not clamp to the boundary here as it may degrade the performance of certain algorithms.
    # one should use `cache_size=1` instead
    return self.atanh(y)

  def log_abs_det_jacobian(self, x, y):
    # We use a formula that is more numerically stable, see details in the following link
    # https://github.com/tensorflow/probability/commit/ef6bb176e0ebd1cf6e25c6b5cecdd2428c22963f#diff-e120f70e92e6741bca649f04fcd907b7
    return 2. * (math.log(2.) - x - F.softplus(-2. * x))


class SquashedNormal(pyd.transformed_distribution.TransformedDistribution):
  def __init__(self, loc, scale):
    self.loc = loc
    self.scale = scale

    self.base_dist = pyd.Normal(loc, scale)
    transforms = [TanhTransform()]
    super().__init__(self.base_dist, transforms)

  @property
  def mean(self):
    mu = self.loc
    for tr in self.transforms:
      mu = tr(mu)
    return mu


class DiagGaussianActor(nn.Module):
  """torch.distributions implementation of an diagonal Gaussian policy."""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth,
               log_std_bounds):
    super().__init__()

    self.log_std_bounds = log_std_bounds
    self.trunk = mlp(obs_dim, hidden_dim, 2 * action_dim, hidden_depth)

    self.outputs = dict()
    self.apply(weight_init)

  def forward(self, obs):
    mu, log_std = self.trunk(obs).chunk(2, dim=-1)

    # constrain log_std inside [log_std_min, log_std_max]
    log_std = torch.tanh(log_std)
    log_std_min, log_std_max = self.log_std_bounds
    log_std = log_std_min + 0.5 * (log_std_max - log_std_min) * (log_std + 1)

    std = log_std.exp()

    self.outputs['mu'] = mu
    self.outputs['std'] = std

    dist = SquashedNormal(mu, std)
    return dist


class MultinomialPolicy(nn.Module):
  """Actor network, for categorical action."""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth, temp):
    super().__init__()

    self.policy = mlp(obs_dim, hidden_dim, action_dim, hidden_depth)
    self.temp = temp

    self.outputs = dict()
    self.apply(weight_init)

  def forward(self, obs):
    logits = self.policy(obs)

    dist = pyd.RelaxedOneHotCategorical(self.temp, logits=logits)

    self.outputs['logits'] = logits

    return dist

  @staticmethod
  def hard_sample(dist):
    soft_action = dist.sample()
    indices = torch.argmax(soft_action, dim=-1)
    hard_action = torch.nn.functional.one_hot(indices, soft_action.shape[1]).to(soft_action)
    return hard_action


class SoftmaxPolicy(nn.Module):
  """Actor network, for TD3."""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth, max_action, policy_noise, noise_clip):
    super().__init__()

    self.policy = mlp(obs_dim, hidden_dim, action_dim, hidden_depth)
    self.max_action = max_action
    self.policy_noise = policy_noise
    self.noise_clip = noise_clip

  def forward(self, obs):
    a = self.policy(obs)
    return self.max_action * torch.tanh(a)

  def noisy_action(self, obs):
    logits = self.forward(obs)

    noise = (
      torch.randn_like(logits) * self.policy_noise
    ).clamp(-self.noise_clip, self.noise_clip)

    action = (
      logits + noise
    ).clamp(-self.max_action, self.max_action)

    return F.softmax(action, dim=-1)

  def hard_action(self, obs):
    probs = self.noisy_action(obs)
    indices = torch.argmax(probs, dim=-1)
    hard_action = torch.nn.functional.one_hot(indices, probs.shape[1]).to(probs)
    return hard_action


class MultinomialPolicyTransformer(nn.Module):
  """Actor network with Transformer, for categorical action."""
  def __init__(self, etce_encoder, len_unit_state, action_dim, len_j_state, d_model, temp):
    super().__init__()

    self.policy = ETCEActor(etce_encoder, len_unit_state, action_dim, len_j_state, d_model)
    self.temp = temp

    self.outputs = dict()

  def forward(self, obs):
    logits = self.policy(obs)

    dist = pyd.RelaxedOneHotCategorical(self.temp, logits=logits)

    self.outputs['logits'] = logits

    return dist

  @staticmethod
  def hard_sample(dist):
    soft_action = dist.sample()
    indices = torch.argmax(soft_action, dim=-1)
    hard_action = torch.nn.functional.one_hot(indices, soft_action.shape[1]).to(soft_action)
    return hard_action


class CategoricalPolicy(nn.Module):
  """Actor network for SACD"""
  def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth):
    super().__init__()

    self.policy = mlp(obs_dim, hidden_dim, action_dim, hidden_depth)
    self.apply(weight_init)

  def forward(self, obs):
    logits = self.policy(obs)
    dist = pyd.Categorical(logits=logits)
    return dist

  @staticmethod
  def hard_sample(dist):
    indices = dist.sample()
    hard_action = torch.nn.functional.one_hot(indices, dist.probs.shape[1]).to(indices)
    return hard_action

  def sample(self, obs):
    logits = self.policy(obs)
    action_probs = F.softmax(logits, dim=-1)
    action_dist = pyd.Categorical(action_probs)
    actions = action_dist.sample().view(-1, 1)

    z = (action_probs == 0.0).float() * 1e-8
    log_action_probs = torch.log(action_probs + z)

    return actions, action_probs, log_action_probs


class MultinomialPolicyMaster(nn.Module):
  """Actor network, for categorical action."""
  def __init__(self, is_state_embedding, action_dim, temp, **kwargs):
    super().__init__()

    # mode: base / path-env & embedding / scheduler-env & etce
    if not is_state_embedding:
      base_args = ["obs_dim", "hidden_depth", "d_model"]
      if not all([x in kwargs for x in base_args]):
        raise KeyError("Some args are missing!")
      self.policy = mlp(kwargs["obs_dim"], kwargs["d_model"],
                        action_dim, kwargs["hidden_depth"])
      self.apply(weight_init)

    elif "embedder" in kwargs:
      self.policy = EmbeddingActor(kwargs["embedder"], action_dim)

    elif "etce_encoder" in kwargs:
      etce_args = ["len_unit_state", "len_j_state", "d_model"]
      if not all([x in kwargs for x in etce_args]):
        raise KeyError("Some args are missing!")
      self.policy = ETCEActor(kwargs["etce_encoder"], kwargs["len_unit_state"],
                              action_dim, kwargs["len_j_state"], kwargs["d_model"])

    else:
      raise NotImplementedError("State embedding is available in two envs: path or jobshop.")

    self.action_dim = action_dim
    self.temp = temp
    self.outputs = dict()

  def forward(self, obs, cnt_feasible=None):
    logits = self.policy(obs)
    if cnt_feasible is not None:
      feasible_action = [[0] * cnt_feasible + [float('-inf')] * (self.action_dim - cnt_feasible)]
      feasible_action = torch.tensor(feasible_action).to(logits.device)
      logits = logits + feasible_action

    dist = pyd.RelaxedOneHotCategorical(self.temp, logits=logits)

    self.outputs['logits'] = logits

    return dist

  @staticmethod
  def hard_sample(dist):
    soft_action = dist.sample()
    indices = torch.argmax(soft_action, dim=-1)
    hard_action = torch.nn.functional.one_hot(indices, soft_action.shape[1]).to(soft_action)
    return hard_action