import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from utils import to_np


def init_orthogonal_weights(m):
  if isinstance(m, nn.Linear):
    orthogonal_init(m.weight)
    nn.init.constant_(m.bias, 0.1)


def orthogonal_init(tensor, gain=1):
  """
  https://github.com/implementation-matters/code-for-paper/blob/094994f2bfd154d565c34f5d24a7ade00e0c5bdb/src/policy_gradients/torch_utils.py#L494
  Fills the input `Tensor` using the orthogonal initialization scheme from OpenAI
  Args:
      tensor: an n-dimensional `torch.Tensor`
      gain: optional scaling factor
  Examples:
      >>> w = torch.empty(3, 5)
      >>> orthogonal_init(w)
  """
  if tensor.ndimension() < 2:
    raise ValueError("Only tensors with 2 or more dimensions are supported")

  rows = tensor.size(0)
  cols = tensor[0].numel()
  flattened = tensor.new(rows, cols).normal_(0, 1)

  if rows < cols:
    flattened.t_()

  # Compute the qr factorization
  u, s, v = torch.svd(flattened, some=True)
  if rows < cols:
    u.t_()
  q = u if tuple(u.shape) == (rows, cols) else v
  with torch.no_grad():
    tensor.view_as(q).copy_(q)
    tensor.mul_(gain)
  return tensor


class CustomNetwork(nn.Module):
  def __init__(self, input_dim, hidden_dims, output_dim, hidden_activation_fn="tanh"):
    super(CustomNetwork, self).__init__()

    self.norm = nn.LayerNorm(input_dim)
    self.input_layer = nn.Linear(input_dim, hidden_dims[0])
    self.hidden_layers = nn.ModuleList()
    for idx in range(len(hidden_dims) - 1):
      self.hidden_layers.append(nn.Linear(hidden_dims[idx], hidden_dims[idx + 1]))
    self.output_layer = nn.Linear(hidden_dims[-1], output_dim)

    if hidden_activation_fn == "tanh":
      self.hfn = torch.tanh
    elif hidden_activation_fn == "relu":
      self.hfn = torch.relu
    else:
      raise NotImplementedError

    self.apply(init_orthogonal_weights)

  def forward(self, state):
    """return estimated value given state"""
    state = self.norm(state)      # (B, S)
    x = self.input_layer(state)   # (B, H1)
    x = self.hfn(x)

    for hidden_layer in self.hidden_layers:
      x = hidden_layer(x)         # (B, Hn)
      x = self.hfn(x)

    x = self.output_layer(x)      # (B, H)
    return x


class RandomNetworkDistillation(nn.Module):
  def __init__(self, input_dim, device, hidden_dims, output_dim, rnd_lr):
    super(RandomNetworkDistillation, self).__init__()
    self.device = device
    self.input_dim = input_dim

    self.predictor = CustomNetwork(input_dim, hidden_dims, output_dim).to(self.device)
    self.target = CustomNetwork(input_dim, hidden_dims, output_dim).to(self.device)
    self.optimizer = torch.optim.Adam(self.predictor.parameters(), lr=rnd_lr)

    self.predictor.train(True)
    self.target.train(False)

  def get_int_reward(self, next_obs):
    # convert state from list to tensor
    next_obs = torch.as_tensor(next_obs, device=self.device).float()
    next_obs = next_obs.reshape(1, -1)

    # get reward
    pred = self.predictor(next_obs)
    target = self.target(next_obs)
    int_reward = F.mse_loss(pred, target).tolist()

    return int_reward

  def update(self, next_obses):
    next_obses = next_obses.reshape(-1, self.input_dim)

    pred = self.predictor(next_obses)
    target = self.target(next_obses)
    loss = F.mse_loss(pred, target)

    self.optimizer.zero_grad()
    loss.backward()
    self.optimizer.step()

    return to_np(loss)
