import numpy as np

from config import Configs


class DispatchingRules(object):
  """Dispatching rules"""

  def __init__(self, len_j_state, mode):
    super().__init__()

    self.len_j_state = len_j_state
    self.mode = mode

  def select_action(self, obs):
    output_dim = int(len(obs) / self.len_j_state)
    pad_dim = int(sum(np.isinf(obs)) / self.len_j_state)
    real_dim = output_dim - pad_dim
    assert real_dim > 0

    j_states = np.reshape(obs, (output_dim, self.len_j_state))[:real_dim]

    if self.mode == Configs.DispatchingRules.RANDOM:
      idx = self._RANDOM(real_dim)
    elif self.mode == Configs.DispatchingRules.FIFO:
      idx = self._FIFO()
    elif self.mode == Configs.DispatchingRules.SPT:
      idx = self._SPT(j_states)
    elif self.mode == Configs.DispatchingRules.MST:
      idx = self._MST(j_states)
    elif self.mode == Configs.DispatchingRules.EDD:
      idx = self._EDD(j_states)
    elif self.mode == Configs.DispatchingRules.CR:
      idx = self._CR(j_states)
    elif self.mode == Configs.DispatchingRules.MDD:
      idx = self._MDD(j_states)
    elif self.mode == Configs.DispatchingRules.ATC:
      idx = self._ATC(j_states)
    else:
      raise ValueError("Wrong mode: {}".format(self.mode))
    return np.eye(output_dim)[idx]

  @staticmethod
  def _RANDOM(cnt):
    """random"""
    return np.random.choice(cnt, 1).item()

  @staticmethod
  def _FIFO():
    """First In First Out"""
    return 0

  @staticmethod
  def _SPT(j_states):
    """Shortest Processing Time"""
    return np.argmin(j_states[:, 0]).item()

  @staticmethod
  def _MST(j_states):
    """Minimum Slack Time"""
    return np.argmin(j_states[:, 2]).item()

  @staticmethod
  def _EDD(j_states):
    """Earliest Due Date"""
    return np.argmin(j_states[:, 4]).item()

  @staticmethod
  def _CR(j_states):
    """Critical Ratio"""
    return np.argmin((j_states[:, 4] - j_states[:, 5]) / j_states[:, 6])

  @staticmethod
  def _MDD(j_states):
    """Modified Due Date"""
    return np.argmin(np.max((j_states[:, 4], j_states[:, 5] + j_states[:, 6]), axis=0))

  @staticmethod
  def _ATC(j_states):
    """Apparent Tardiness Cost"""
    k = 1.2
    x = 1 / j_states[:, 0]
    y = np.maximum(0, j_states[:, 4] - j_states[:, 0] - j_states[:, 8])
    z = k * np.mean(j_states[:, 0])
    return np.argmax(x * np.exp(-y/z))

