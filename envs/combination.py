import torch
import numpy as np
from copy import deepcopy

from .utils import Environment


class Combination(Environment):
  """Simulator to find the combination of rock scissors paper
    - 10번 가위바위보를 해서 (가위, 바위, 보) 횟수가 (2, 5, 3)이 되도록 학습
    - delayed reward
  """
  def __init__(self, myseed, penalty, final_reward, goal):
    super(Combination, self).__init__()
    self.verbose = True
    self.initial = [0, 0, 0]
    self.action_sample = [1, 0, 0]  # (scissors, rock, paper)

    self.penalty = penalty
    self.final_reward = final_reward
    self.goal = goal
    self.myseed = myseed

    self.state = None
    self.reset()

  @property
  def observation_shape(self):
    return np.array(self.initial).shape

  @property
  def action_shape(self):
    return np.array(self.action_sample).shape

  def next(self, policy):
    """Return next step"""
    state = deepcopy(self.state)
    action = policy.select_action(state)
    next_state = self.transition(state, action)
    reward = self.observe_reward(next_state)
    done = 0 if sum(next_state) < 10 else 1
    replay_memory_set = (
      state, action, reward, next_state, done
    )

    self.state = deepcopy(next_state)

    return replay_memory_set

  def reset(self):
    """reset state to initial"""
    self.state = deepcopy(self.initial)
    np.random.seed(self.myseed)
    if self.set_torch_seed:
      torch.manual_seed(self.myseed)
      torch.cuda.manual_seed_all(self.myseed)
    return

  @staticmethod
  def transition(state, action):
    """Return next state
    args:
      state: [1, 2, 3]
      action: [0, 1, 0]
    """
    if not isinstance(action, list):
      action = action.astype(np.int).tolist()

    s = deepcopy(state)
    s = [int(x) + int(y) for x, y in zip(s, action)]
    return s

  def observe_reward(self, state):
    """Check if state is in feasible space"""
    if sum(state) < 10:
      return 0      # temporal reward
    elif state == self.goal:
      return self.final_reward
    return self.penalty