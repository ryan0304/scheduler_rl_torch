import numpy as np
import pandas as pd
import plotly.express as px
import torch
from datetime import datetime, timedelta

from .utils import Environment


class Job(object):
  """
  All jobs carry their information for Reinforcement Learning
  Status of a job agent
    1) in queue
    2) in machine

    selected: from 1 to 2
    processed: from 2 to (1 or out)
  """

  def __init__(self, id_, seq, d_t, a_t, t_p_t_list, p_t_j, len_j_state):
    """
    id_: ID of JOB or MACHINE | e.g. 'J0', 'J1', 'J2', ...
    seq: processing sequence of job | e.g. [0,2,1]: 0 stage -> 2 stage -> 1 stage
    d_t: due date | e.g. 15
    t_p_t_list: estimated total remaining processing time | e.g. [4+2+5=11,2+5=7,5] if p_t is [4,2,5]
    p_t_j: processing time | e.g. 5

    a_t: arrival time in queue | e.g. 3
    c_seq: current sequence of job | e.g. job is in seq[c_seq]
    t_p_t: sum of total remaining processing time | e.g. 10
    hist: history of job agent | e.g. [[machine ID, arrival time, waiting time], ...]
    """
    # predefined variables
    self.ID = id_
    self.seq = seq
    self.d_t = d_t
    self.initial = a_t
    self.t_p_t_list = t_p_t_list
    self.p_t_j = p_t_j
    self.seq_length = len(seq)
    self.len_j_state = len_j_state

    # changeable variables
    self.a_t = a_t
    self.c_seq = 0
    self.c_stage = self._update_c_stage()
    self.t_p_t = self._update_t_p_t()
    self.hist = []

  def selected(self, now, m_id):
    """job is now processed by machine"""
    self.hist.append([m_id, self.a_t, now - self.a_t])
    return

  def processed(self, now, stocker_list, finish):
    """job is now finished by machine"""
    self.a_t = now
    self.c_seq += 1

    if self.is_not_finish:
      self.c_stage = self._update_c_stage()
      self.t_p_t = self._update_t_p_t()
      stocker_list[self.c_stage].append(self)
      return

    finish.append(self)
    return

  @property
  def is_not_finish(self):
    return self.c_seq < self.seq_length

  def get_j_state(self, now):
    """
    1) processing time of job
    2) waiting time
    3) slack_time
    4) remain_sequence
    --> to ratio: 1) w_t/p_t, 2) s_t/p_t, 3) remain_ratio
    """
    # ret = [self.p_t_j, now - self.a_t, self.d_t - now - self.t_p_t, self.seq_length - self.c_seq]
    ret = [(now - self.a_t) / self.p_t_j, (self.d_t - now - self.t_p_t) / self.p_t_j, 1 - self.c_seq/self.seq_length]
    assert len(ret) == self.len_j_state
    return ret

  def get_dispatching_state(self, now):
    """
    1) processing time of job
    2) waiting time
    3) slack time
    4) remain_sequence
    5) due date
    6) current time
    7) total remaining processing time
    8) current_sequence
    9) now
    """
    ret = [self.p_t_j, now - self.a_t, self.d_t - now - self.t_p_t, self.seq_length - self.c_seq,
           self.d_t, now, self.t_p_t, self.c_seq, now]
    return ret

  def _update_t_p_t(self):
    return self.t_p_t_list[self.c_seq]

  def _update_c_stage(self):
    return self.seq[self.c_seq]


class Machine(object):
  """
  All machines carry their information for Reinforcement Learning
  Refresh p_cycle after definition
    Status of a machine agent
    1) work
    2) idle

    work: from 2 to 1
    idle: from 1 to 2
  """

  def __init__(self, id_, p_t_m, len_m_state):
    """
    ID: ID of JOB or MACHINE | e.g. M0,M1,M2,... (string)
    p_t_m: processing time of the machine | e.g. 5

    c_t: completion time of a current job | e.g. current time(2) + processing time(5) = 7
    wip: class or None, information of a job in process or no job in process | e.g. JOB class or None
    hist: list, history of machine agent | e.g. [[job ID, start time, completion time], ...]
    """
    # predefined variables
    self.ID = id_
    self.p_t_m = p_t_m
    self.len_m_state = len_m_state
    self.alpha = 0.7    # related to bottleneck index
    self.p_cycle = 0    # related to bottleneck index

    # changeable variables
    self.c_t = 0
    self.wip = None
    self.util = 0
    self.b_i = 0    # not real time
    self.working_list = []    # for calculating b_i
    self.hist = []

    # previous states
    self.p_states = []

  @property
  def p_t(self):
    if self.wip is None:
      raise ImportError("Cannot get p_t if there is no wip")
    return self.wip.p_t_j + self.p_t_m

  def work(self, now, j_info):
    self.wip = j_info
    self.c_t = now + self.p_t
    self.working_list.extend([0] * (now - len(self.working_list)))
    self.hist.append([j_info.ID, now, self.c_t])
    return

  def idle(self, now):
    self.working_list.extend([1] * self.p_t)
    self.b_i = self.calc_b_i(now)
    self.wip = None
    return

  def get_m_state(self, now):
    """
    1) processing time of machine
    2) completion time - now
    3) bottleneck_index
    --> to ratio: 1) (c_t-now)/p_t, 2) b_i
    """
    # ret = [self.p_t_m, max(0, self.c_t - now), self.b_i]
    ret = [max(0, self.c_t - now) / self.p_t_m, self.b_i]
    assert len(ret) == self.len_m_state
    return ret

  def calc_b_i(self, now):
    """for real time, use method named refresh_bottleneck_index"""
    if len(self.working_list) < 1:
      return 0
    util = np.mean(self.working_list)
    if (self.p_cycle == 0) or (now < self.p_cycle):
      return util
    recent_util = np.mean(self.working_list[-self.p_cycle:])
    self.util = util
    return (1 - self.alpha) * util + self.alpha * recent_util

  def refresh_p_cycle(self, p_cycle):
    self.p_cycle = p_cycle

  def refresh_bottleneck_index(self, now):
    """
    calculate bottleneck index without using self.working_list
    BI = (1-alpha) * utilization + alpha * recent_utilization

    now: look "action"
    return: bottleneck index | e.g. 0.8
    """
    # calculate utilization
    if (now == 0) or (len(self.hist) < 1):
      return 0

    s_p_t = 0  # sum of the processing time
    for _, s_t, c_t in self.hist:
      if now >= c_t:
        # when ith job has finished
        s_p_t += (c_t - s_t)
      elif now >= s_t:
        # when ith job is in process
        s_p_t += (now - s_t)
      else:
        raise Exception('There is an unknown problem while calculating BI...')
    util = s_p_t / now

    # calculate recent_utilization
    recent = now - self.p_cycle
    if recent < 0:
      return util

    r_s_p_t = 0  # removal sum of the processing time
    for _, s_t, c_t in self.hist:
      # ignore the processing time before the time of 'recent'
      if recent > c_t:
        r_s_p_t += (c_t - s_t)
      elif recent > s_t:
        r_s_p_t += (recent - s_t)
        break
      else:
        break
    recent_util = (s_p_t - r_s_p_t) / self.p_cycle

    # calculate BI
    b_i = (1 - self.alpha) * util + self.alpha * recent_util
    self.util = util
    return b_i


class JobShopScheduling(Environment):
  """
  <JOB SHOP SIMULATOR>
          stage0    stage1      ...         stage ns
          [ ]       [ ]         [ ]         [ ]
  oo      [ ]       [ ]         [ ]         [ ]
                                [ ]
  i job / j machine
  job = 0, 1, ..., i, ..., nj
  stage = 0, 1, ..., k, ..., ns
  machine = 0, 1, ..., j, ..., nm_k

  <Assumptions>
    - Jobs have a different processing sequence (job1: stage 1-2-3, job2: stage 3-1-2).
    - Jobs do not have to pass all stages.
    - There are parallel machines in one stage.
    - Jobs/Machines have a different processing time.
    - Jobs are stored in stocker with infinite size, and travel time of job is ignored.
    - There is no intentional break for machines.
    - Recirculation is not considered.
  """

  def __init__(self, m_type, j_type, n_jobs, n_initial_jobs, stocker_capacity, _beta, verbose,
               myseed, len_j_state, len_m_state, len_state_seq, len_dispatching_state, len_compare_state):
    """
    m_type: the number of machines for each stage | e.g. [1,3,2]
    j_type: sequence type of job | e.g. {'type0':[0,1,2]}
    n_jobs: the number of jobs to generate | e.g. 1000
    n_initial_jobs: the number of jobs at initial point
    stocker_capacity: capacity of stocker to "handle" | e.g. 100
    _beta: mean = std = beta = 1 / lambda | e.g. 10
    verbose: verbose of class job and machine | e.g. True or False
    myseed: random seed of numpy | e.g. 304
    fix_seed: if False, myseed += 1 when initialize

    len_j_state: length of j_state | e.g. 4
    len_m_state: length of m_state | e.g. 3

    n_seq: the number of job types | e.g. 4
    n_stage: the number of stages | e.g. 3
    """
    super(JobShopScheduling, self).__init__()

    # simulator
    self.m_type = m_type
    self.j_type = j_type
    self.n_jobs = n_jobs
    self.n_initial_jobs = n_initial_jobs
    self.stocker_capacity = stocker_capacity
    self._beta = _beta
    self.verbose = verbose
    self.myseed = myseed
    self.len_j_state = len_j_state
    self.len_m_state = len_m_state
    self.len_state_seq = len_state_seq
    self.len_dispatching_state = len_dispatching_state
    self.len_compare_state = len_compare_state

    # check len_global_state when modifying state
    self.len_global_state = 0 if self.use_dispatching_state else 3 + len_m_state * 3
    self.len_unit_state = self.len_global_state + len_j_state * stocker_capacity
    proposed_state = self.len_unit_state * len_state_seq
    compare_state = len_compare_state + len_dispatching_state * stocker_capacity
    self.len_state = compare_state if self.use_compare_state else proposed_state
    self.n_seq = len(self.j_type)
    self.n_stage = len(self.m_type)
    self.n_machine = sum(self.m_type)

    # initialization (look reset)
    self.now, self.todo_list, self.finished, self.stocker_list, self.state_list = None, None, None, None, None
    self.flow_time, self.machines, self.machines_with_min_p_t, self.g = None, None, None, None
    self.reset()

    # shape of obs & action
    self.pad_token = [np.inf, 0][1]     # np.inf
    self._observation_shape = (self.len_state,)
    self._action_shape = (stocker_capacity,)

  @property
  def observation_shape(self):
    return self._observation_shape

  @property
  def action_shape(self):
    return self._action_shape

  def next(self, policy):
    """Return next step"""
    ret = "return of while loop (None if finished else m_info)"
    while len(self.todo_list) > 0:
      if self.verbose:
        string_todo = " | ".join(["(%s, %s)" % (time_, id_) for id_, time_, _ in self.todo_list])
        stocker_info = str(tuple(len(x) for x in self.stocker_list))
        print("| {} | {} | {} | {} |".format(self.now, stocker_info, len(self.finished), string_todo))
      id_, time_, job_or_machine = self.todo_list.pop(0)
      self.now = time_

      if id_.startswith('J'):
        j_info = job_or_machine
        stage_to_process = j_info.c_stage

        # job moves to stocker
        self.stocker_list[stage_to_process].append(j_info)
        self._append_idle_machine_to_todo_list(stage_to_process)

        # append new job to todo_list
        try:
          j_info = next(self.g)
          self._update_todo_list(j_info, j_info.a_t)
        except StopIteration:
          pass

      elif id_.startswith('M'):
        m_info = job_or_machine

        # wip moves to stocker
        if m_info.wip is not None:
          j_info = m_info.wip
          m_info.idle(self.now)
          j_info.processed(self.now, self.stocker_list, self.finished)
          if j_info.is_not_finish:
            self._append_idle_machine_to_todo_list(j_info.c_stage, m_info.ID)
          if len(self.finished) >= self.n_jobs:
            ret = None
            break

        # check stocker
        c_stage, _ = self._find_target_machine(m_info)
        if len(self.stocker_list[c_stage]) > 0:
          # action with policy outside while loop
          ret = (c_stage, m_info)
          break

      else:
        raise ValueError('Wrong ID: %s', id_)

    if type(ret) is str:
      # impossible
      raise RecursionError

    elif ret is None:
      # if done scheduling
      m_id, state, action, reward, now, done = None, None, None, None, 0, 1

    else:
      # if not done scheduling
      c_stage, m_info = ret
      if hasattr(policy, 'mode'):
        # dispatching rule
        state = self.to_dispatching_state(self.now, self.stocker_list[c_stage])
        action = policy.select_action(state)
        state = self.to_sequential_state(self.now, m_info, self.stocker_list[c_stage])
      elif hasattr(policy, 'step'):
        # comparison method
        state = self.to_comparison_state(self.now, self.stocker_list[c_stage])
        action = policy.select_action(state)
      else:
        # proposed reinforcement learning: no 'mode' or 'step'
        state = self.to_sequential_state(self.now, m_info, self.stocker_list[c_stage])
        cnt_feasible = self.count_nonpad_job(state)
        action = policy.select_action(state, cnt_feasible=cnt_feasible)
      reward = -sum([len(x) for x in self.stocker_list]) / self.n_jobs
      m_id = m_info.ID
      done = 0

      # job to machine
      j_info = self.stocker_list[c_stage].pop(np.argmax(action).item())
      m_info.work(self.now, j_info)
      j_info.selected(self.now, m_id)
      self._update_todo_list(m_info, m_info.c_t)

    replay_memory_set = (m_id, state, action, reward, self.now, done)

    return replay_memory_set

  def reset(self):
    """
    now: current time | e.g. 3
    to_do_list: to do list | e.g. [tuple_to_do0, tuple_to_do1, ...]
    finished: saving finished jobs | e.g. [job_info0, ...]
    stocker_list: stocker per stage | e.g. [stocker_stage0, stocker_stage1, ...]
    flow_time:
    """
    self.now = 0
    self.todo_list = []
    self.finished = []
    self.stocker_list = [[] for _ in range(self.n_stage)]
    self.state_list = [[[] for _ in range(self.m_type[i])] for i in range(self.n_stage)]
    self.flow_time = []
    np.random.seed(self.myseed)
    if self.set_torch_seed:
      torch.manual_seed(self.myseed)
      torch.cuda.manual_seed_all(self.myseed)

    if self.use_semiconductor_fab:
      self.machines, self.machines_with_min_p_t = self._semiconductor_fab_generator()
    else:
      self.machines, self.machines_with_min_p_t = self._machine_generator()
    self._initialize_p_cycle(self.machines)
    self.g = self._job_generator()
    j_info = next(self.g)
    self._update_todo_list(j_info, j_info.a_t)

    return

  def update_seed(self):
    self.myseed += 1
    np.random.seed(self.myseed)
    if self.set_torch_seed:
      torch.manual_seed(self.myseed)
      torch.cuda.manual_seed_all(self.myseed)
    return

  def to_state(self, now, m_info, stocker):
    """construct state
    (1) global info
    (2) m_info with minimum c_t in current stage
    (3) m_info with minimum p_t in current stage
    (4) m_info with minimum c_t
    (5) m_info with maximum b_i
    (6) m_info
    (7) j_info
    """
    if len(stocker) < 1:
      raise ValueError("Stocker is empty!")

    # (1) global info
    finish_ratio = len(self.finished) / self.n_jobs
    avg_stocker_length = np.mean([len(x) for x in self.stocker_list]) / self.stocker_capacity
    avg_b_i = np.mean([x.b_i for y in self.machines for x in y])    # not real time
    state = [finish_ratio, avg_stocker_length, avg_b_i]

    # find current stage and idx
    c_stage, idx = self._find_target_machine(m_info)

    # (2) machine info with minimum completion time in current stage
    m_infos_same_stage = self.machines[c_stage]
    if len(m_infos_same_stage) == 1:
      m_state = m_info.get_m_state(now)
    else:
      without = [x.get_m_state(now) for i, x in enumerate(m_infos_same_stage) if i != idx]
      m_state = without[np.argmin([x[1] for x in without]).item()]    # x[1] is completion time
    state += m_state

    # # (3) machine info with minimum processing time in the current stage
    # state += self.machines_with_min_p_t[c_stage].get_m_state(now)

    # (4) machine info with minimum completion time
    whole_machines = [x for y in self.machines for x in y]
    # c_t_list = [(x.c_t, i) for i, x in enumerate(whole_machines) if x.c_t > 0]
    # if c_t_list:
    #   m_state = whole_machines[min(c_t_list)[1]].get_m_state(now)
    # else:
    #   m_state = m_info.get_m_state(now)
    # state += m_state

    # (5) machine info with maximum bottleneck index
    b_i_list = [(x.b_i, i) for i, x in enumerate(whole_machines)]
    state += whole_machines[max(b_i_list)[1]].get_m_state(now)

    # (6) machine info
    state += m_info.get_m_state(now)

    assert len(state) == self.len_global_state

    # (7) stocker info
    stocker_to_handle = stocker[:self.stocker_capacity]
    j_state = [x for y in stocker_to_handle for x in y.get_j_state(now)]
    pad_length = self.stocker_capacity * stocker[0].len_j_state - len(j_state)
    j_state += [self.pad_token] * pad_length
    state += j_state

    assert len(state) == self.len_unit_state

    return state

  def to_dispatching_state(self, now, stocker):
    """for dispatching rules"""
    stocker_to_handle = stocker[:self.stocker_capacity]
    state = [x for y in stocker_to_handle for x in y.get_dispatching_state(now)]
    return state

  def to_comparison_state(self, now, stocker):
    """construct state for comparison method
    Shu Luo, Dynamic scheduling for flexible job shop~ (2020)
    (1) average utilization of all machines
    (2) std of utilization
    (3) average completion rate
    (4) average completion rate 2
    (5) std of completion rate 2
    (6) estimated average number of tardy jobs
    (7) current average number of tardy jobs
    """
    if len(stocker) < 1:
      raise ValueError("Stocker is empty!")

    utils = [x.util for y in self.machines for x in y]
    U_ave = np.mean(utils)    # (1)
    U_std = np.std(utils)     # (2)

    jobs = self._get_all_jobs()
    OP_i, n_i, T_left, D_i = zip(*[(x.c_seq, x.seq_length, x.t_p_t, x.d_t) for x in jobs])
    CRJ = [x / y for x, y in zip(OP_i, n_i)]

    CRO_ave = sum(OP_i) / sum(n_i)    # (3)
    CRJ_ave = np.mean(CRJ)    # (4)
    CRJ_std = np.std(CRJ)     # (5)

    N_tard_est, N_tard_act, N_left = 0, 0, 0
    for x, y, z, w in zip(OP_i, n_i, T_left, D_i):
      if x < y:
        N_left += (y - x)
        if now + y > w:
          N_tard_est += (y - x)
        if now > w:
          N_tard_act += (y - x)

    Tard_e = N_tard_est / N_left    # (6)
    Tard_a = N_tard_act / N_left    # (7)

    state = [U_ave, U_std, CRO_ave, CRJ_ave, CRJ_std, Tard_e, Tard_a]
    assert len(state) == self.len_compare_state

    dispatching_state = self.to_dispatching_state(now, stocker)
    min(len(stocker), self.stocker_capacity)
    pad_length = max(self.stocker_capacity - len(stocker), 0) * self.len_dispatching_state
    dispatching_state += [self.pad_token] * pad_length

    state += dispatching_state
    assert len(state) == self.len_state

    return state

  def to_sequential_state(self, now, m_info, stocker):
    if self.use_dispatching_state:
      current_state = self.to_dispatching_state(now, stocker)
    else:
      current_state = self.to_state(now, m_info, stocker)
    c_stage, idx = self._find_target_machine(m_info)

    # modify state_list
    self.state_list[c_stage][idx].append(current_state)
    self.state_list[c_stage][idx] = self.state_list[c_stage][idx][-self.len_state_seq:]

    # get sequential state
    state = [x for y in self.state_list[c_stage][idx][::-1] for x in y]
    pad_length = self.len_state - len(state)
    state += [self.pad_token] * pad_length

    assert len(state) == self.len_state

    return state

  def count_nonpad_job(self, state):
    if self.use_compare_state:
      j_state = state[self.len_compare_state:]
      j_state = np.reshape(j_state, (-1, self.len_dispatching_state))
    else:
      j_state = state[self.len_global_state:self.len_unit_state]
      j_state = np.reshape(j_state, (-1, self.len_j_state))
    cnt = np.sum(np.all(j_state == self.pad_token, axis=-1))
    return self.stocker_capacity - int(cnt)

  @property
  def c_max(self):
    return self.now

  @property
  def f_bar(self):
    flow_times = [j_info.a_t - j_info.initial for j_info in self.finished]
    return np.mean(flow_times).item()

  @property
  def l_bar(self):
    lateness = [j_info.a_t - j_info.d_t for j_info in self.finished]
    return np.mean(lateness).item()

  @property
  def t_bar(self):
    tardiness = [max(j_info.a_t - j_info.d_t, 0) for j_info in self.finished]
    return np.mean(tardiness).item()

  def plot_gantt_chart(self):
    """
    plot gantt chart using plotly
    legend information is not perfect...
    return: None, just plot gantt chart
    """
    # get history of machines
    m_hists = {cl.ID: cl.hist for mset in self.machines for cl in mset}

    # convert m_hists to df of plotly format
    today = datetime.today()

    df = [dict(Task=m_id,
               Start=str(today + timedelta(minutes=s_t)),
               Finish=str(today + timedelta(minutes=f_t)),
               Resource=job_id)
          for m_id, m_hist in m_hists.items() for job_id, s_t, f_t in m_hist]

    # plot gantt chart
    fig = px.timeline(pd.DataFrame(df), x_start="Start", x_end="Finish", y="Task", color="Resource")
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(autorange="reversed")
    fig.show()

  def _machine_generator(self):
    """
    generate classes of machines if "self.m_type" is decided
    return: list, machine classes | e.g. [[m0, m1], [m2, m3, m4], [m5]] if m_type is [2,3,1]
    """
    machines, p_t_list = [], []
    m_id = 0
    id_formatter = "M{0:>0%dd}" % len(str(self.n_machine))

    for i in range(self.n_stage):
      m_set, p_t_stage = [], []   # machine set for each stage
      n_machine = self.m_type[i]
      for j in range(n_machine):
        id_ = id_formatter.format(m_id)
        p_t_m = max(1, int(np.random.normal(self._beta*n_machine, self._beta/6)))
        m_set.append(Machine(id_=id_, p_t_m=p_t_m, len_m_state=self.len_m_state))
        p_t_stage.append(p_t_m)
        m_id += 1
      machines.append(m_set)
      p_t_list.append(p_t_stage)

    min_p_t_m_list = [machines[i][np.argmin(x).item()] for i, x in enumerate(p_t_list)]

    return machines, min_p_t_m_list

  def _semiconductor_fab_generator(self):
    """
    generate semiconductor fab
    return: list, machine classes | e.g. [[m0, m1], [m2, m3, m4], [m5]] if m_type is [2,3,1]
    """
    machines, p_t_list = [], []
    m_id = 0
    id_formatter = "M{0:>0%dd}" % len(str(self.n_machine))

    p_t_m_s = [8, 25, 27, 23, 31, 39, 31, 22, 24, 20, 39, 31, 21, 39, 4, 15, 8, 18, 69, 27, 38, 5, 11, 39]
    p_t_m_s = [x - self._beta/2 for x in p_t_m_s]
    for i in range(self.n_stage):
      m_set, p_t_stage = [], []   # machine set for each stage
      n_machine = self.m_type[i]
      for j in range(n_machine):
        id_ = id_formatter.format(m_id)
        p_t_m = max(1, int(np.random.normal(p_t_m_s[i], self._beta/6)))
        m_set.append(Machine(id_=id_, p_t_m=p_t_m, len_m_state=self.len_m_state))
        p_t_stage.append(p_t_m)
        m_id += 1
      machines.append(m_set)
      p_t_list.append(p_t_stage)

    min_p_t_m_list = [machines[i][np.argmin(x).item()] for i, x in enumerate(p_t_list)]

    return machines, min_p_t_m_list

  def _job_generator(self):
    """
    Considerations of Job_Generator are as follow:
      - generating time
      - size of a job
      - processing sequence of a job
    return: the information of job | e.g. job.keys=['seq', 'd_t', 'a_t', 't_p_t_list']
    """
    seq_set = list(self.j_type.values())
    t_p_t_m_lists = self._calc_t_p_t_m()
    id_formatter = "J{0:>0%dd}" % len(str(self.n_jobs))

    n_jobs = self.n_jobs if self.is_static_scheduling else self.n_jobs * 5
    n_initial_jobs = self.n_jobs if self.is_static_scheduling else self.n_initial_jobs
    for i in range(n_jobs):
      p_t_j = int(np.random.uniform(low=1, high=self._beta))    # processing time
      seq_id = np.random.choice(self.n_seq, 1).item()   # select type of job

      # t_p_t_list (p_t = p_t_m + p_t_j)
      t_p_t_m_list = t_p_t_m_lists[seq_id]
      seq_length = len(t_p_t_m_list)
      t_p_t_list = [x + p_t_j*(seq_length-i) for i, x in enumerate(t_p_t_m_list)]

      # margin for d_t
      margin = int(t_p_t_list[0] * np.random.uniform(1.5, 3))

      if i < n_initial_jobs:
        a_t = 0
        d_t = np.random.uniform(low=margin, high=margin*self.n_jobs)
      else:
        arrival_interval = int(np.random.exponential(scale=self._beta))
        a_t = self.now + arrival_interval
        d_t = a_t + margin

      j_info = Job(id_=id_formatter.format(i),
                   seq=seq_set[seq_id],
                   d_t=d_t,
                   a_t=a_t,
                   t_p_t_list=t_p_t_list,
                   p_t_j=p_t_j,
                   len_j_state=self.len_j_state)

      yield j_info

  def _initialize_p_cycle(self, machines):
    """
    update production cycle for calculating recent utilization
    return: scalar, the maximum production cycle | e.g. 8+30+20=58
    """
    seq_set = list(self.j_type.values())
    avg_p_t_per_stage = [int(np.mean([cl.p_t_m for cl in m_set]) + 0.5*self._beta)
                         for m_set in self.machines]   # avg_machine_p_t + avg_job_p_t
    pts_list = [sum([avg_p_t_per_stage[i] for i in seq]) for seq in seq_set]
    p_cycle = max(pts_list)

    for m_set in machines:
      for m_info in m_set:
        m_info.refresh_p_cycle(p_cycle)
    return

  def _calc_t_p_t_m(self):
    """
    calculate total remaining processing time of machine at each stage
    result = [list_0, list_1, ..., list_(n_seq)]
    list_0: list, estimated total remaining processing time of job agent |
        e.g. [4+2+5=11,2+5=7,5] if p_t_seq is [4,2,5]
    return: list, look above information
    """
    seq_set = list(self.j_type.values())
    avg_p_t_m_per_stage = [int(np.mean([cl.p_t_m for cl in m_set])) for m_set in self.machines]
    result = []
    for i in range(self.n_seq):
      p_t_seq = [avg_p_t_m_per_stage[j] for j in seq_set[i]]    # change order by sequence
      result.append([sum(p_t_seq[i:]) for i in range(len(p_t_seq))])
    return result

  def _update_todo_list(self, info, time):
    """
    1) append
    2) sort (prioritize jobs over machines when having same processing time)
    return: None, just update self variables
    """
    # append
    self.todo_list.append((info.ID, time, info))

    # sort
    self.todo_list.sort(key=lambda x: x[0])
    self.todo_list.sort(key=lambda x: x[1])

  def _find_target_machine(self, m_info):
    """
    find target machine when machine ID is given
    return: (c_stage, idx): current stage index and machine index in stage |
            e.g. (2, 1) means '1'th machine in '2'th stage
    """
    IDs = [[m.ID for m in stage] for stage in self.machines]
    for c_stage in range(len(IDs)):
      for idx in range(len(IDs[c_stage])):
        if IDs[c_stage][idx] == m_info.ID:
          return c_stage, idx
    raise Exception('Cannot find (c_stage, idx)!')

  def _append_idle_machine_to_todo_list(self, stage_idx, ignore_id=None):
    """
    1) find all idle machines related to "stage_idx"
    2) append idle machines to todo_list
    """
    m_infos = self.machines[stage_idx]
    todo_id_list = [x[0] for x in self.todo_list]
    for m_info in m_infos:
      if m_info.ID == ignore_id:
        continue
      if m_info.ID not in todo_id_list:
        self._update_todo_list(m_info, self.now)
    return

  def _get_all_jobs(self):
    in_stocker = [x for y in self.stocker_list for x in y]
    in_machine = [x.wip for y in self.machines for x in y if x.wip is not None]
    finished = [x for x in self.finished]
    return in_stocker + in_machine + finished
