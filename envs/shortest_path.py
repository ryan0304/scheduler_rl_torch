import torch
import numpy as np
import plotly.graph_objects as go
from copy import deepcopy


from .utils import Environment


class ShortestPath(Environment):
  """Simulator to find shortest path
    - 5 by 5 grid
    - 좌하단: (0, 0) / 우상단: (4, 4)
    - (0, 0)에서 (4, 4)까지 장애물을 피해 빠른 길로 가는 방법 학습
    - 장애물: (1, 0) / (1, 2) / (3, 2) / (3, 3)
  """
  def __init__(self, myseed, two, is_obstacles):
    super(ShortestPath, self).__init__()
    self.verbose = True
    self.two = two
    self.is_obstacles = is_obstacles
    self.grid_size = 5
    self.initial = [0, 0]
    self.terminal = [self.grid_size-1, self.grid_size-1]
    self.action_sample = [1, 0] if self.two else [0, 1, 0, 0]
    self.obstacles = [[1, 0], [1, 2], [3, 2], [3, 3]] if self.is_obstacles else []

    self.final_reward = 0
    self.out_penalty = -50
    self.max_space = max(self.terminal)
    self.myseed = myseed

    self.state = None
    self.visit = {(x, y): 0 for x in range(self.grid_size) for y in range(self.grid_size)}
    self.visit['out'] = 0
    self.reset()

  @property
  def observation_shape(self):
    return np.array(self.initial).shape

  @property
  def action_shape(self):
    return np.array(self.action_sample).shape

  def next(self, policy):
    """Return next step"""
    state = deepcopy(self.state)
    action = policy.select_action(state)
    next_state = self.transition(state, action)
    reward = self.observe_reward(next_state)
    done = 1 if reward in [self.final_reward, self.out_penalty] else 0
    replay_memory_set = (
      state, action, reward, next_state, done
    )

    self.state = deepcopy(next_state)
    key_ = tuple(next_state) if tuple(next_state) in self.visit else 'out'
    self.visit[key_] += 1

    return replay_memory_set

  def reset(self):
    """reset state to initial"""
    self.state = deepcopy(self.initial)
    self.visit[(0, 0)] += 1
    np.random.seed(self.myseed)
    if self.set_torch_seed:
      torch.manual_seed(self.myseed)
      torch.cuda.manual_seed_all(self.myseed)
    return

  def transition(self, state, action):
    """Return next state
    args:
      state: [1, 2]
      action: [0, 1, 0, 0]
    """
    if not isinstance(action, list):
      action = action.astype(np.int).tolist()

    flag = action.index(1)
    s = deepcopy(state)

    if self.two and flag == 0:
      s[0] += 1
    elif self.two and flag == 1:
      s[1] += 1
    elif not self.two and flag == 0:
      s[0] -= 1
    elif not self.two and flag == 1:
      s[1] += 1
    elif not self.two and flag == 2:
      s[1] -= 1
    elif not self.two and flag == 3:
      s[0] += 1
    else:
      raise ValueError("Wrong action:", action)
    return s

  def observe_reward(self, state):
    """Check if state is in feasible space"""
    if state == self.terminal:
      return self.final_reward
    elif state in self.obstacles:
      return self.out_penalty
    elif all(0 <= x <= self.max_space for x in state):
      return (sum(state) - 8) / 30
    return self.out_penalty

  def draw_heatmap(self, is_save, img_path):
    # visit = {(x, y): np.random.randint(20) for x in range(5) for y in range(5)}
    # heatmapData = [[visit[(x, y)] for x in range(5)] for y in range(5)]
    heatmapData = [[self.visit[(x, y)] for x in range(self.grid_size)] for y in range(self.grid_size)]
    fig = go.Figure(data=go.Heatmap(
      z=heatmapData,
      xgap=10,
      ygap=10,
      colorscale="Blues"
    ))
    fig.add_trace(
      go.Scatter(mode='markers',
                 x=[0], y=[0],
                 marker=dict(color='peachpuff', size=20,
                             symbol=['square-dot'],
                             line=dict(color='sandybrown', width=2)),
                 name="starting point",
                 showlegend=True)
    )
    fig.add_trace(
      go.Scatter(mode='markers',
                 # x=[self.grid_size-1], y=[self.grid_size-1],
                 x=[4], y=[4],
                 marker=dict(color='peachpuff', size=20,
                             symbol=['diamond-dot'],
                             line=dict(color='sandybrown', width=2)),
                 name="arrival point",
                 showlegend=True)
    )
    show_legend = True
    for coord in self.obstacles:
      fig.add_trace(
        go.Scatter(mode='markers',
                   x=[coord[0]], y=[coord[1]],
                   marker=dict(color='palevioletred', size=30,
                               symbol=['hexagon-dot'],
                               line=dict(color='crimson', width=4)),
                   name="obstacles",
                   showlegend=show_legend)
      )
      show_legend=False
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)
    fig.update_layout(
      width=600, height=600,
      legend=dict(orientation='h', x=0, y=1.1))
    if is_save:
      fig.write_image(img_path,
                      engine="kaleido")
    else:
      fig.show()
    return