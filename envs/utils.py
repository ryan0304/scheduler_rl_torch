import abc
from config import Configs


class Environment(object):
  def __init__(self):
    self.set_torch_seed = Configs.ExperimentConfig.set_torch_seed
    self.is_static_scheduling = Configs.ExperimentConfig.is_static_scheduling
    self.use_dispatching_state = Configs.ExperimentConfig.use_dispatching_state
    self.use_compare_state = Configs.ExperimentConfig.use_compare_state
    self.use_semiconductor_fab = Configs.ExperimentConfig.use_semiconductor_fab

  @abc.abstractmethod
  def observation_shape(self):
    """Shape of observation (property)."""
    pass

  @abc.abstractmethod
  def action_shape(self):
    """Shape of action (property)."""
    pass

  @abc.abstractmethod
  def next(self, policy):
    """Return (s, a, r, sp, d) and move to next step using policy."""
    pass

  @abc.abstractmethod
  def reset(self):
    """Reset state to initial."""
    pass