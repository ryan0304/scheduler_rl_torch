import numpy as np
import pickle
import torch


def to_np(t):
  if t is None:
    return None
  elif t.nelement() == 0:
    return np.array([])
  else:
    return t.cpu().detach().numpy()


class RunningMeanStd(object):
  # https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Parallel_algorithm
  # https://github.com/openai/random-network-distillation
  def __init__(self, buffer_path, epsilon=1e-4, shape=()):
    self.buffer_path = buffer_path
    self.shape = shape
    self.epsilon = epsilon
    self.mean = np.zeros(shape, 'float64')
    self.var = np.ones(shape, 'float64')
    self.count = epsilon

  def update(self, x):
    batch_mean, batch_std, batch_count = np.mean(x, axis=0), np.std(x, axis=0), x.shape[0]
    batch_var = np.square(batch_std)
    self.update_from_moments(batch_mean, batch_var, batch_count)

  def update_from_moments(self, batch_mean, batch_var, batch_count):
    delta = batch_mean - self.mean
    tot_count = self.count + batch_count

    new_mean = self.mean + delta * batch_count / tot_count
    m_a = self.var * self.count
    m_b = batch_var * batch_count
    M2 = m_a + m_b + np.square(delta) * self.count * batch_count / (self.count + batch_count)
    new_var = M2 / (self.count + batch_count)

    new_count = batch_count + self.count

    self.mean = new_mean
    self.var = new_var
    self.count = new_count

  def transform(self, x):
    return (x - self.mean) / np.sqrt(self.var)

  def update_transform(self, x):
    self.update(x)
    return self.transform(x)

  def initialize(self):
    self.mean = np.zeros(self.shape, 'float64')
    self.var = np.ones(self.shape, 'float64')
    self.count = self.epsilon

  def save(self, filename=None):
    filename = filename if filename else self.buffer_path
    with open(filename, 'wb') as f:
      f.write(pickle.dumps(self.__dict__))

  def load(self, filename=None):
    filename = filename if filename else self.buffer_path
    with open(filename, 'rb') as f:
      data_pickle = f.read()
    self.__dict__ = pickle.loads(data_pickle)


class RunningStat:
  """
  https://www.johndcook.com/blog/standard_deviation/
  """

  def __init__(self):
    self.__m_n = 0
    self.__m_oldM = 0
    self.__m_newM = 0
    self.__m_oldS = 0
    self.__m_newS = 0

  def reset(self):
    self.__m_n = 0

  def push(self, x):
    self.__m_n += 1

    if self.__m_n == 1:
      self.__m_oldM = x
      self.__m_newM = x
      self.__m_oldS = 0.
    else:
      self.__m_newM = self.__m_oldM + (x - self.__m_oldM) / self.__m_n
      self.__m_newS = self.__m_oldS + (x - self.__m_oldM) * (x - self.__m_newM)

      # set up for next iteration
      self.__m_oldM = self.__m_newM
      self.__m_oldS = self.__m_newS

  def get_num_data_values(self):
    return self.__m_n

  def mean(self):
    if self.__m_n > 0:
      return self.__m_newM
    else:
      return 0

  def variance(self):
    if self.__m_n > 1:
      return self.__m_newS / (self.__m_n - 1)
    else:
      return 1.

  def std(self):
    return np.sqrt(self.variance())

  def load_variables(self, saved_dict):
    self.__m_n = saved_dict['n']
    self.__m_oldM = saved_dict['mu']
    self.__m_newM = saved_dict['mu']
    self.__m_oldS = saved_dict['var']
    self.__m_newS = saved_dict['var']

  def save_variables(self):
    return {"n": self.__m_n, "mu": self.__m_newM, "var": self.__m_newS}


class RewardScaler:
  def __init__(self, gamma=1., scaler_path="./checkpoints/scaler.pth"):
    self.rs = RunningStat()
    self.gamma = gamma
    self.scaler_path = scaler_path

  def __call__(self, r_t, update=True):
    if update:
      R_t = self.gamma * self.rs.std() + r_t
      self.rs.push(R_t)
    return r_t / (self.rs.std() + 1e-5)

  def save(self, path=None):
    path = path if path else self.scaler_path
    torch.save(self.rs.save_variables(), path)

  def load(self, path=None):
    path = path if path else self.scaler_path
    self.rs.load_variables(torch.load(path))


class ObservationScaler:
  def __init__(self, scaler_path="./checkpoints/obs_scaler.pth"):
    self.rs = RunningStat()
    self.scaler_path = scaler_path

  def __call__(self, ob_t, update=True):
    if update:
      self.rs.push(ob_t)
    return (ob_t - self.rs.mean()) / (self.rs.std() + 1e-5)

  def save(self, path=None):
    path = path if path else self.scaler_path
    torch.save(self.rs.save_variables(), path)

  def load(self, path=None):
    path = path if path else self.scaler_path
    self.rs.load_variables(torch.load(path))


def tt(x):
  """for debug"""
  return torch.tensor(x).float().reshape(1, -1)