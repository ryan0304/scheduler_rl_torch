import numpy as np
from datetime import datetime
from copy import copy
import wandb
import pdb

from config import Configs
from agent import TD3Agent, SoftActorCriticAgent, SoftActorCriticDiscreteAgent, DQNAgent, ReplayBuffer, DispatchingRules
from envs import Combination, ShortestPath, JobShopScheduling
from utils import tt
from train.experiment import Experiment


class Trainer(Experiment):
  """RL Trainer"""
  def __init__(self, myseed, n_episode, print_stats_frequency, save_frequency, is_wandb,
               debug_frequency, plot_frequency):
    super(Trainer, self).__init__()
    self.env = None
    if self.environment == "path":
      self.env = ShortestPath(myseed=myseed, **Configs.ShortestPathConfig.to_dict())
    elif self.environment == "combination":
      self.env = Combination(myseed=myseed, **Configs.CombinationConfig.to_dict())
    elif self.environment == "jobshop":
      self.env = JobShopScheduling(myseed=myseed, **Configs.JobShopConfig.to_dict())
    else:
      raise ValueError("Available environment for TrainSample: 'path', 'combination', and 'jobshop'.")

    self.replay_buffer = ReplayBuffer(self.env.observation_shape,
                                      self.env.action_shape,
                                      n_episode,
                                      **Configs.ReplayBufferConfig.to_dict())
    self.agent = None
    if self.agent_mode == Configs.RLAgent.TD3:
      len_global_state, len_j_state, len_unit_state = None, None, None
      if self.environment == "jobshop":
        len_global_state = self.env.len_global_state
        len_j_state = self.env.len_j_state
        len_unit_state = self.env.len_unit_state
      self.agent = TD3Agent(obs_dim=self.env.observation_shape[0],
                            action_dim=self.env.action_shape[0],
                            len_global_state=len_global_state,
                            len_j_state=len_j_state,
                            len_unit_state=len_unit_state,
                            **Configs.TD3MasterConfig.to_dict())
    elif self.agent_mode == Configs.RLAgent.SAC:
      self.agent = SoftActorCriticAgent(obs_dim=self.env.observation_shape[0],
                                        action_dim=self.env.action_shape[0],
                                        **Configs.SACConfigBase.to_dict())
    elif self.agent_mode == Configs.RLAgent.SACD:
      self.agent = SoftActorCriticDiscreteAgent(obs_dim=self.env.observation_shape[0],
                                                action_dim=self.env.action_shape[0],
                                                **Configs.SACDConfigBase.to_dict())
    elif self.agent_mode == Configs.RLAgent.DQN:
      self.agent = DQNAgent(obs_dim=self.env.observation_shape[0],
                            len_dispatching_state=self.env.len_dispatching_state,
                            len_compare_state=self.env.len_compare_state,
                            **Configs.DQNConfig.to_dict())
    else:
      raise ValueError("Available agent mode: %s" % ', '.join(Configs.RLAgent.to_dict().values()))

    self.fifo, self.dr = None, None
    if self.environment == "jobshop":
      self.fifo = DispatchingRules(len_j_state=self.env.len_dispatching_state,
                                   mode=Configs.DispatchingRules.FIFO)

      self.dr = DispatchingRules(len_j_state=self.env.len_dispatching_state,
                                 mode=Configs.DispatchingRules.MST)

    if self.is_loading:
      self.agent.load_model()

    self.n_episode = n_episode
    self.print_stats_frequency = print_stats_frequency
    self.save_frequency = save_frequency
    self.is_wandb = is_wandb
    self.debug_frequency = debug_frequency
    self.plot_frequency = plot_frequency

    # for jobshop only
    self.best_pm = np.inf
    self.best_memory = None
    self.best_ratio = 0.2
    self.plot_histogram = False

    if self.is_wandb:
      wandb.init(project=Configs.WanDBConfig.PROJECT,
                 name="%s_%s" % (Configs.WanDBConfig.NAME, datetime.now().strftime("%Y-%m-%d_%H:%M:%S")))
      wandb.config.environment = Configs.ExperimentConfig.environment
      wandb.config.actor_temp = Configs.TD3MasterConfig.actor_temp
      wandb.config.d_model = Configs.TD3MasterConfig.d_model
      wandb.config.n_jobs = Configs.JobShopConfig.n_jobs
      wandb.config.is_static_scheduling = Configs.ExperimentConfig.is_static_scheduling
      wandb.config.is_credit_assignment = Configs.ExperimentConfig.is_credit_assignment
      wandb.config.curiosity_mode = Configs.ExperimentConfig.curiosity_mode
      wandb.config.agent_mode = Configs.ExperimentConfig.agent_mode
      wandb.config.sampling_mode = Configs.ExperimentConfig.sampling_mode
      wandb.config.use_gae = Configs.ExperimentConfig.use_gae
      wandb.config.is_state_embedding = Configs.ExperimentConfig.is_state_embedding
      wandb.config.mode = Configs.WanDBConfig.MODE
      wandb.save(Configs.WanDBConfig.PATH)

  def fit(self):
    is_jobshop = self.environment == "jobshop"
    if self.do_warmup and is_jobshop:
      self.warm_up(**Configs.WarmUpConfig.to_dict())

    iteration, n_epi = 0, 0
    for n_epi in range(self.n_episode):
      # Initialize
      start = datetime.now()
      n_step, done = 0, 0
      if is_jobshop and not self.fix_seed:
        self.env.update_seed()
      self.env.reset()

      states, break_flag = [], False    # for sample
      ret_rl, ret_fifo, ret_dr, flow_times = None, None, None, None   # for jobshop
      if is_jobshop:
        tmp_buffer = {}
      else:
        tmp_buffer = []
        states.append(self.env.state)

      # Execute run, store memory
      tic = datetime.now()
      while done < 1:
        if is_jobshop:
          m_id, state, action, reward, now, done = self.env.next(self.agent)
          tmp_buffer.setdefault(m_id, []).append((state, action, reward, now))
        else:
          replay_memory_set = self.env.next(self.agent)
          tmp_buffer.append(replay_memory_set)
          done = replay_memory_set[-1]
          states.append(self.env.state)

        iteration += 1
        n_step += 1

        # Set max length for path environment
        if self.environment == "path" and n_step > 50:
          break_flag = True
          break
      running_time = datetime.now() - tic

      if is_jobshop:
        ret_rl = self.pm
        flow_times = [x.a_t - x.initial for x in self.env.finished]
        if self.do_gantt_plot and n_epi == 0:
          self.env.plot_gantt_chart()
        if (ret_fifo is None) or (not self.fix_seed):
          ret_fifo, ret_dr = self.get_dispatching_result()

      # Apply delayed reward and Store replay memory for combination environment
      if self.environment == "path":
        final_reward = self.env.out_penalty if break_flag else None    # delayed penalty for looping path
      elif self.environment == "combination":
        final_reward = tmp_buffer[-1][2]              # delayed reward
      elif self.environment == "jobshop":
        min_dr = min(ret_fifo, ret_dr)                # take minimum of dispatching rules
        final_reward = self._calc_reward(ret_rl, min_dr)
      else:
        return ValueError("Set delayed reward for the environment %s!" % self.environment)

      # Handling tmp_buffer
      tic = datetime.now()
      if not is_jobshop:
        actor = self.agent.actor if self.agent_mode == Configs.RLAgent.SACD else None
        tmp_buffer, sum_reward, sum_intrinsic = self.agent.handle_replay_buffer(
          tmp_buffer, n_epi, self.agent.critic_target, final_reward, actor)
      elif self.agent_mode == Configs.RLAgent.DQN:
        tmp_buffer, sum_reward = self.agent.handle_compare_buffer(tmp_buffer, self.env)
        sum_intrinsic = 0
      else:
        tmp_buffer, sum_reward, sum_intrinsic = self.agent.handle_jobshop_buffer(
          tmp_buffer, final_reward, self.env, n_epi)

      # Append memory to replay buffer
      for tup_ in tmp_buffer:
        self.replay_buffer.add(*tup_)

      # Add best memory to replay memory
      if is_jobshop:
        self.add_best_memory(ret_rl, tmp_buffer, n_epi)
      buffer_time = datetime.now() - tic

      # Update
      tic = datetime.now()
      q_loss, p_loss, a_loss, r_loss, v_loss, q_values = [], [], [], [], [], []
      for k_ in range(n_step):
        # Update RL
        ret = self.agent.update(self.replay_buffer, iteration, n_epi, k_, n_step)
        if self.agent_mode == Configs.RLAgent.TD3:
          critic_loss, actor_loss, int_loss, value_loss, q_value = ret
          alpha_loss = 0
        elif self.agent_mode in [Configs.RLAgent.SAC, Configs.RLAgent.SACD]:
          critic_loss, actor_loss, alpha_loss, value_loss, q_value, alpha = ret
          int_loss = 0
        elif self.agent_mode == Configs.RLAgent.DQN:
          critic_loss, int_loss, q_value = ret
          actor_loss, alpha_loss, value_loss = 0, 0, 0
        else:
          raise ValueError("Wrong agent mode!")

        q_loss.append(critic_loss)
        p_loss.append(actor_loss)
        a_loss.append(alpha_loss)
        r_loss.append(int_loss)
        v_loss.append(value_loss)
        q_values.append(q_value)
      update_time = datetime.now() - tic

      # Log to wandb
      q_loss = np.mean(q_loss).item()
      p_loss = np.nanmean(p_loss).item()
      a_loss = np.nanmean(a_loss).item()
      r_loss = np.nanmean(r_loss).item()
      v_loss = np.nanmean(v_loss).item()
      q_values = np.mean(q_values).item()

      if self.is_wandb:
        wandb_log = {"q_loss": q_loss, "p_loss": p_loss, "a_loss": a_loss,
                     "v_loss": v_loss, "r_loss": r_loss,
                     "q_value": q_values, "iteration": iteration}
        if is_jobshop:
          wandb_log.update({"rl": ret_rl, "dr": ret_dr, "fifo": ret_fifo})
          if self.plot_histogram:
            wandb_log.update({"flow_time": wandb.Histogram(flow_times)})
        wandb.log(wandb_log)

      if n_epi % self.print_stats_frequency == 0:
        self.print_logs(is_jobshop, n_epi, states, start, sum_reward, sum_intrinsic, final_reward,
                        q_loss, p_loss, v_loss, a_loss, r_loss, ret_rl, ret_fifo, ret_dr,
                        running_time, buffer_time, update_time)

      if self.debug_frequency and (n_epi % self.debug_frequency == 0):
        # from pprint import pprint; pprint(self.env.visit)
        pdb.set_trace()
        _ = tt([0, 0])

      if self.environment == "path" and (n_epi % self.plot_frequency == 0):
        img_path = "/Users/ryan/Downloads/shortestPath/sample_%d.png" % n_epi
        self.env.draw_heatmap(True, img_path)

      if n_epi % self.save_frequency == 0:
        self.save_all(iteration)

    # Save final model
    self.save_all(iteration)
    if self.debug_frequency:
      if self.environment == "path":
        img_path = "/Users/ryan/Downloads/shortestPath/sample_%d.png" % n_epi
        self.env.draw_heatmap(True, img_path)
      pdb.set_trace()

  def save_all(self, iteration):
    self.agent.save_model(iteration)
    self.replay_buffer.save()

  def _calc_reward(self, ret_rl, ret_dr):
    """
    ret_rl: performance measure using rl policy
    ret_dr: performance measure using dispatching rule
    backup: ret = (-0.1) * np.exp((ret_rl/ret_dr)**_beta)
    """
    _beta = [2, 10][0]
    with_best = True
    is_simple = True

    if is_simple:
      return -3 * np.exp(-(ret_dr/ret_rl)**_beta)
    elif ret_dr < ret_rl:
       return False    # when ret_dr < ret_rl, use immediate reward
    elif with_best:
       return np.clip((-0.1) * np.exp((ret_rl/self.best_pm)**_beta), -_beta, 0)
    return -5 * np.exp(-(ret_dr/ret_rl)**_beta)
    # return -3.5 * (ret_rl/ret_dr) + 2

  def warm_up(self, pretrain_path, pre_iteration, pre_epoch, load_model):
    """initialize agent with dispatching rule"""
    # @TODO: add tmp_buffer to replay_buffer
    if self.agent_mode != Configs.RLAgent.TD3:
      raise NotImplementedError("Only TD3 is available in jobshop environment.")

    if load_model:
      self.agent.load_model(pretrain_path)
      return

    init_seed = self.env.myseed
    iter_ = 0

    for iter_ in range(pre_iteration):
      done, tmp_buffer = 0, []
      self.env.reset()
      while done < 1:
        m_id, state, action, reward, now, done = self.env.next(self.dr)
        if (state is not None) and (action is not None):
          tmp_buffer.append((state, action))

      pre_loss = self.agent.pre_update_actor(self.replay_buffer, pre_epoch)
      if self.is_wandb:
        wandb.log({"pre_loss": pre_loss})

      self.env.update_seed()

    if pre_iteration > 0:
      self.agent.save_model(iter_, pretrain_path)
    self.env.myseed = init_seed

    return

  def get_dispatching_result(self):
    done = 0
    self.env.reset()
    while done < 1:
      done = self.env.next(self.fifo)[-1]
    ret_fifo = self.pm

    done = 0
    self.env.reset()
    while done < 1:
      done = self.env.next(self.dr)[-1]
    ret_dr = self.pm

    return ret_fifo, ret_dr

  def add_best_memory(self, ret_rl, tmp_buffer, n_epi):
    if ret_rl < self.best_pm:
      self.best_pm = ret_rl
      self.best_memory = copy(tmp_buffer)
    if self.best_ratio > 0:
      div = int(1 / self.best_ratio)
      if n_epi % div == 0:
        for replay_memory_set in self.best_memory:
          self.replay_buffer.add(*replay_memory_set)
    return

  @staticmethod
  def print_logs(is_jobshop, n_epi, states, start, sum_reward, sum_intrinsic, final_reward,
                 q_loss, p_loss, v_loss, a_loss, r_loss, ret_rl, ret_fifo, ret_dr,
                 running_time, buffer_time, update_time):
    print('#' * 100)
    print('[ EPISODE %d ]' % n_epi)
    print('  - elapsed time: %s' % (datetime.now() - start))
    print('  - elapsed time for run: %s' % running_time)
    print('  - elapsed time for buffer: %s' % buffer_time)
    print('  - elapsed time for update: %s' % update_time)
    if not is_jobshop:
      print('  - path: %s' % ' -> '.join([str(tuple(x)) for x in states]))
      print('  - the length of path: %d' % len(states))
    else:
      print('  - performance measure of RL   : {}'.format(ret_rl))
      print('  - performance measure of FIFO : {}'.format(ret_fifo))
      print('  - performance measure of DR   : {}'.format(ret_dr))
    print('  - reward: %.2f / intrinsic_reward: %.2f / final_reward: %.4f' % (sum_reward, sum_intrinsic, final_reward))
    print('  - losses: critic=%.3f / actor=%.3f' % (q_loss, p_loss))
    print('  - losses: value=%.3f / alpha=%.3f / intrinsic=%.3f' % (v_loss, a_loss, r_loss))
    return

  @property
  def pm(self):
    """performance measure, only for jobshop"""
    if self.environment != "jobshop":
      raise ValueError("Function 'pm' is available in jobshop environment")
    if self.is_static_scheduling:
      return self.env.c_max
    return self.env.f_bar


if __name__ == '__main__':
  trainer = Trainer(**Configs.TrainConfig.to_dict())
  trainer.fit()
