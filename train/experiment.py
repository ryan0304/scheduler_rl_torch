from config import Configs


class Experiment(object):
  """variables of experiments"""
  def __init__(self):
    super(Experiment, self).__init__()
    self.is_static_scheduling = Configs.ExperimentConfig.is_static_scheduling
    self.fix_seed = Configs.ExperimentConfig.fix_seed
    self.environment = Configs.ExperimentConfig.environment
    self.agent_mode = Configs.ExperimentConfig.agent_mode
    self.rnd_flag = Configs.ExperimentConfig.curiosity_mode
    self.use_gae = Configs.ExperimentConfig.use_gae
    self.do_warmup = Configs.ExperimentConfig.do_warmup
    self.do_gantt_plot = Configs.ExperimentConfig.do_gantt_plot
    self.is_loading = Configs.ExperimentConfig.is_loading