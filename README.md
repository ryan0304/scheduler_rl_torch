# Deep Reinforcement Learning implementation in PyTorch

Two RL algorithms are developed based on OpenAI Spinning Up (https://spinningup.openai.com/en/latest/index.html):
- SAC: Soft Actor-Critic
- TD3: Twin Delayed Deep Deterministic Policy Gradient

One main environments and two sample environments are developed:
- main: job shop
- sample: shortest path, combination

## Run

Run main
```
python3 main.py --mode main
```

Run sample
```
python3 main.py --mode sample
```

## Directory
* `./agent/` : RL algorithms and networks of actor and critic
* `./backups/` : backup codes
* `./config/` : configuration of experiments
* `./envs/` : two problems
    - shortest_path: find path from (0, 0) to (4, 4) with some obstacles
    - combination: match final combination (2, 5, 3)
* `./models/` : deep neural networks in pytorch
* `./tests/` : test codes
