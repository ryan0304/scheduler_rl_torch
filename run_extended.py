import wandb
from tqdm import tqdm
from datetime import datetime
from config import Configs
from envs import JobShopScheduling
from agent import TD3Agent, DispatchingRules, DQNAgent


def runEnv(scheduler, return_cmax=False):
  done = 0
  env.reset()

  while done < 1:
    done = env.next(scheduler)[-1]

  if return_cmax:
    return env.c_max
  return env.f_bar


if __name__ == "__main__":
  # Environment
  env = JobShopScheduling(myseed=Configs.TrainConfig.myseed, **Configs.JobShopConfig.to_dict())

  # Dispatching rules
  rand = DispatchingRules(len_j_state=env.len_dispatching_state,
                          mode=Configs.DispatchingRules.RANDOM)
  fifo = DispatchingRules(len_j_state=env.len_dispatching_state,
                          mode=Configs.DispatchingRules.FIFO)
  mst = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.MST)
  edd = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.EDD)
  cr = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.CR)
  mdd = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.MDD)
  atc = DispatchingRules(len_j_state=env.len_dispatching_state,
                         mode=Configs.DispatchingRules.ATC)

  # Reinforcement learning agent
  do_compare = True
  if not do_compare:
    agent = TD3Agent(obs_dim=env.observation_shape[0],
                     action_dim=env.action_shape[0],
                     len_global_state=env.len_global_state,
                     len_j_state=env.len_j_state,
                     len_unit_state=env.len_unit_state,
                     **Configs.TD3MasterConfig.to_dict())
  else:
    agent = DQNAgent(obs_dim=env.observation_shape[0],
                     len_dispatching_state=env.len_dispatching_state,
                     len_compare_state=env.len_compare_state,
                     **Configs.DQNConfig.to_dict())

  # Load
  agent.load_model()
  wandb.init(project=Configs.WanDBConfig.PROJECT,
             name="%s_%s" % (Configs.WanDBConfig.NAME, datetime.now().strftime("%Y-%m-%d_%H:%M:%S")))
  wandb.config.n_jobs = Configs.JobShopConfig.n_jobs
  wandb.save(Configs.WanDBConfig.PATH)

  # Run
  ruleNames = ["RL", "RANDOM", "FIFO", "MST", "EDD", "CR", "MDD", "ATC"]
  rules = [agent, rand, fifo, mst, edd, cr, mdd, atc]

  for i in tqdm(range(100)):
    # initialize
    env.update_seed()

    summaryDic = dict()
    for policy, key_ in zip(rules, ruleNames):
      performance = runEnv(policy)
      summaryDic[key_] = performance

    wandb.log(summaryDic)


  # # box plot
  # import pandas as pd
  # import plotly.graph_objs as go
  # df = pd.DataFrame({"td3": fbars_td3, "mst": fbars_mst, "fifo": fbars_fifo})
  # # df.to_csv("run_100times.csv")
  #
  # pdb.set_trace()
  # fig = go.Figure()
  # fig.add_trace(go.Box(y=df["td3"], name="proposed method", marker_color="lightseagreen"))
  # fig.add_trace(go.Box(y=df["mst"], name="mst", marker_color="indianred"))
  # fig.add_trace(go.Box(y=df["fifo"], name="fifo", marker_color="darkblue"))
  # fig.show()

