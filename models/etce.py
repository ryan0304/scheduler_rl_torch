import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer

PAD_TOKEN = np.inf


class GlobalClusterEmbedding(nn.Module):

  def __init__(self, len_global_state, num_embeddings, d_model):
    super(GlobalClusterEmbedding, self).__init__()
    self.clustering = nn.Linear(len_global_state, num_embeddings)
    self.embedding = nn.Linear(num_embeddings, d_model)

    self.init_weights()

  def forward(self, global_state):
    global_state[global_state == PAD_TOKEN] = 0                     # (B, len_global_state)
    clustered = F.softmax(self.clustering(global_state), dim=-1)    # (B, num_embeddings)
    embedded = self.embedding(clustered)                            # (B, d_model)
    return embedded

  def init_weights(self):
    initrange = 0.1
    self.clustering.weight.data.uniform_(-initrange, initrange)
    self.clustering.bias.data.zero_()
    self.embedding.weight.data.uniform_(-initrange, initrange)
    self.embedding.bias.data.zero_()


class QueueClusterEmbedding(nn.Module):

  def __init__(self, len_j_state, num_embeddings, d_model):
    super(QueueClusterEmbedding, self).__init__()
    self.layer = TransformerEncoderLayer(len_j_state, 1, 32, 0.)
    self.clustering = nn.Linear(len_j_state, num_embeddings)
    self.embedding = nn.Linear(num_embeddings, d_model)

    self.init_weights()

  def forward(self, queue_state):
    # queue_state: (B, stocker_capacity, 3)
    src_key_padding_mask = queue_state.sum(dim=-1).eq(PAD_TOKEN)  # (B, stocker_capacity)

    # no job in stocker
    loc = src_key_padding_mask.eq(True).all(dim=-1)   # (B,)
    src_key_padding_mask[loc] = torch.zeros_like(src_key_padding_mask[loc])

    # input
    queue_state[queue_state == PAD_TOKEN] = 0    # (B, stocker_capacity, 3)
    queue_state = queue_state.transpose(0, 1)         # (stocker_capacity, B, 3)
    pad_handled = self.layer(queue_state, src_key_padding_mask=src_key_padding_mask)

    # clustering and embedding
    clustered = F.softmax(self.clustering(pad_handled), dim=-1)   # (stocker_capacity, B, num_embeddings)
    embedded = self.embedding(clustered)                          # (stocker_capacity, B, d_model)
    embedded = embedded.transpose(0, 1)                           # (B, stocker_capacity, d_model)

    return embedded

  def init_weights(self):
    initrange = 0.1
    self.clustering.weight.data.uniform_(-initrange, initrange)
    self.clustering.bias.data.zero_()
    self.embedding.weight.data.uniform_(-initrange, initrange)
    self.embedding.bias.data.zero_()


class ETCEEncoder(nn.Module):

  def __init__(self, len_global_state, len_j_state, len_unit_state,
               num_embeddings, d_model, num_heads, dff, num_layers, dropout=0.1):
    super(ETCEEncoder, self).__init__()
    self.len_global_state = len_global_state
    self.len_j_state = len_j_state
    self.len_unit_state = len_unit_state
    self.stocker_capacity = (len_unit_state - len_global_state) // len_j_state
    self.d_model = d_model
    self.output = None    # (S, B, d_model)

    self.global_ce = GlobalClusterEmbedding(len_global_state, num_embeddings, d_model)
    self.queue_ce = QueueClusterEmbedding(len_j_state, num_embeddings, d_model)
    self.embedding = nn.Embedding(3, d_model)   # 0/1: for padding, 2: for special token
    encoder_layers = TransformerEncoderLayer(d_model, num_heads, dff, dropout)
    self.transformer_encoder = TransformerEncoder(encoder_layers, num_layers)

    self.init_weights()

  def init_weights(self):
    initrange = 0.1
    self.embedding.weight.data.uniform_(-initrange, initrange)

  def forward(self, src):
    # shape
    batch, Sp = src.shape       # (B, len_state)
    seq = Sp // self.len_unit_state   # seq: len_state_seq

    # split global and queue information
    for_ce = src.reshape(-1, self.len_unit_state)             # (B * seq, len_state)
    global_src = for_ce[:, :self.len_global_state].clone()    # (B * seq, len_global_state)
    queue_src = for_ce[:, self.len_global_state:].clone()     # (B * seq, stocker_capacity * len_j_state)

    # pad location
    global_pad = global_src.sum(dim=-1).eq(PAD_TOKEN).view(batch, seq, 1)   # (B, seq, 1)
    queue_pad = queue_src.view(
      batch, seq, self.stocker_capacity, -1).sum(dim=-1).eq(PAD_TOKEN)      # (B, seq, stocker_capacity)
    pad_loc = torch.cat((global_pad, queue_pad), dim=-1)                    # (B, seq, 1 + stocker_capacity)

    # apply ClusterEmbeddings
    queue_src = queue_src.view(-1, self.stocker_capacity, self.len_j_state)   # (B * seq, stocker_capacity, len_j_state)
    global_src = self.global_ce(global_src)     # (B * seq, d_model)
    queue_src = self.queue_ce(queue_src)        # (B * seq, stocker_capacity, d_model)

    # reshape and merge embeddings
    global_src = global_src.view(batch, seq, 1, self.d_model)
    queue_src = queue_src.view(batch, seq, self.stocker_capacity, self.d_model)
    merged = torch.cat((global_src, queue_src), dim=2)    # (B, seq, 1+stocker_capacity, d_model)

    # convert pad
    embed = torch.cat((torch.zeros((batch, seq, 1), dtype=torch.long),
                       torch.ones((batch, seq, self.stocker_capacity), dtype=torch.long)),
                      dim=-1).to(src.device)
    embed = self.embedding(embed)     # (B, seq, 1+stocker_capacity) --> (B, seq, 1+stocker_capacity, d_model)
    embed = torch.where(pad_loc.unsqueeze(-1), embed, merged)

    # append summary token
    spt = (torch.ones((batch, 1), dtype=torch.long) * 2).to(src.device)
    spt = self.embedding(spt)
    embed = torch.cat((spt, embed.view(batch, -1, self.d_model)), dim=1)    # (B, S, d_model)
    embed = embed.transpose(0, 1)                                           # (S, B, d_model)
    spt_pad = torch.zeros((batch, 1), dtype=torch.bool).to(src.device)
    src_key_padding_mask = torch.cat((spt_pad, pad_loc.view(batch, -1)), dim=-1)    # (B, S)

    # output
    output = self.transformer_encoder(embed, src_key_padding_mask=src_key_padding_mask)
    self.output = output

    # (S, B, d_model) --> (B, d_model)
    return output[0]


class ETCEActor(nn.Module):

  def __init__(self, etce_encoder, len_unit_state, action_dim, len_j_state, d_model):
    super(ETCEActor, self).__init__()
    self.len_unit_state = len_unit_state
    self.action_dim = action_dim
    self.len_j_state = len_j_state

    self.encoder = etce_encoder
    self.dense1 = nn.Linear(d_model, d_model)
    self.dense2 = nn.Linear(d_model, d_model)
    self.out = nn.Linear(d_model, action_dim)

    self.init_weights()

  def init_weights(self):
    initrange = 0.1
    self.dense1.bias.data.zero_()
    self.dense1.weight.data.uniform_(-initrange, initrange)
    self.dense2.bias.data.zero_()
    self.dense2.weight.data.uniform_(-initrange, initrange)
    self.out.bias.data.zero_()
    self.out.weight.data.uniform_(-initrange, initrange)

  def forward(self, src):
    # find feasible action
    current = src[:, :self.len_unit_state]
    src_key_padding_mask = current.eq(PAD_TOKEN)
    inf_count = src_key_padding_mask.sum(dim=-1)    # (B,)
    inf_count = [int(x/self.len_j_state) for x in inf_count.tolist()]
    assert self.action_dim not in inf_count

    feasible_action = [[0] * (self.action_dim - x) + [-np.inf] * x for x in inf_count]
    feasible_action = torch.tensor(feasible_action).to(src.device)    # (B, A)

    # output: logits
    output = self.encoder(src)            # (B, d_model)
    output = F.relu(self.dense1(output))  # (B, d_model)
    output = F.relu(self.dense2(output))  # (B, d_model)
    output = self.out(output)           # (B, A)
    output += feasible_action             # (B, A)

    return output


class ETCECritic(nn.Module):

  def __init__(self, etce_encoder, action_dim, d_model):
    super(ETCECritic, self).__init__()
    self.action_dim = action_dim
    self.encoder = etce_encoder
    self.embedding = nn.Linear(action_dim, d_model)
    self.dense = nn.Linear(d_model * 2, d_model * 2)
    self.out = nn.Linear(d_model * 2, 1)

    self.init_weights()

  def init_weights(self):
    initrange = 0.1
    self.embedding.bias.data.zero_()
    self.embedding.weight.data.uniform_(-initrange, initrange)
    self.dense.bias.data.zero_()
    self.dense.weight.data.uniform_(-initrange, initrange)
    self.out.bias.data.zero_()
    self.out.weight.data.uniform_(-initrange, initrange)

  def forward(self, src):
    # input
    obs = src[:, :-self.action_dim]
    action = src[:, -self.action_dim:]
    obs = self.encoder(obs)         # (B, d_model)
    act = self.embedding(action)    # (B, d_model)

    # output
    output = torch.cat((obs, act), dim=1)   # (B, d_model*2)
    output = F.relu(self.dense(output))     # (B, d_model*2)
    output = self.out(output)               # (B, 1)

    return output


class ETCEValue(nn.Module):

  def __init__(self, etce_encoder, d_model):
    super(ETCEValue, self).__init__()
    self.encoder = etce_encoder
    self.dense = nn.Linear(d_model, d_model)
    self.out = nn.Linear(d_model, 1)

    self.init_weights()

  def init_weights(self):
    initrange = 0.1
    self.dense.bias.data.zero_()
    self.dense.weight.data.uniform_(-initrange, initrange)
    self.out.bias.data.zero_()
    self.out.weight.data.uniform_(-initrange, initrange)

  def forward(self, obs):
    src = self.encoder(obs)             # (B, d_model)
    output = F.relu(self.dense(src))    # (B, d_model)
    output = self.out(output)           # (B, 1)
    return output