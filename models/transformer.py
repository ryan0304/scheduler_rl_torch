import torch
import numpy as np
import torch.nn as nn
from torch.nn import TransformerEncoder, TransformerEncoderLayer


class TransformerActor(nn.Module):

  def __init__(self, action_dim, d_model, num_heads, dff, num_layers, len_j_state, len_global_state, dropout=0.5):
    super(TransformerActor, self).__init__()
    self.action_dim = action_dim
    self.len_j_state = len_j_state
    self.len_global_state = len_global_state
    self.d_model = d_model

    self.expander = nn.Linear(1, d_model)
    encoder_layers = TransformerEncoderLayer(d_model, num_heads, dff, dropout)
    self.transformer_encoder = TransformerEncoder(encoder_layers, num_layers)
    self.extractor = nn.Linear(d_model * len_j_state, 1)
    self.final_layer = nn.Linear(action_dim, action_dim)

    self.init_weights()

  def init_weights(self):
    initrange = 0.1
    self.expander.bias.data.zero_()
    self.expander.weight.data.uniform_(-initrange, initrange)
    self.extractor.bias.data.zero_()
    self.extractor.weight.data.uniform_(-initrange, initrange)
    self.final_layer.bias.data.zero_()
    self.final_layer.weight.data.uniform_(-initrange, initrange)

  def forward(self, src):
    # handle mask
    src_key_padding_mask = src.eq(np.inf)     # (B, S)
    inf_count = src_key_padding_mask.sum(dim=-1)    # (B,)
    inf_count = [int(x/self.len_j_state) for x in inf_count.tolist()]
    feasible_action = [[0] * (self.action_dim - x) + [-np.inf] * x for x in inf_count]
    feasible_action = torch.tensor(feasible_action).to(src.device)    # (B, A)

    # input
    src[src == np.inf] = 0                    # (B, S)
    src = src.permute(1, 0).unsqueeze(-1)     # (S, B, 1)
    src = self.expander(src)                  # (S, B, D)

    # output
    output = self.transformer_encoder(src, src_key_padding_mask=src_key_padding_mask)
    output = output[self.len_global_state:]       # (A*J, B, D)
    output = output.transpose(0, 1)               # (B, A*J, D)
    output = output.reshape((-1, self.action_dim, self.len_j_state * self.d_model))   # (B, A, J*D)
    output = self.extractor(output).squeeze(-1)   # (B, A)
    output = self.final_layer(output)             # (B, A)
    output += feasible_action                     # (B, A)

    return output


class TransformerCritic(nn.Module):

  def __init__(self, d_model, num_heads, dff, num_layers, dropout=0.5):
    super(TransformerCritic, self).__init__()
    self.d_model = d_model

    self.embedding = nn.Embedding(1, d_model)
    self.expander = nn.Linear(1, d_model)
    encoder_layers = TransformerEncoderLayer(d_model, num_heads, dff, dropout)
    self.transformer_encoder = TransformerEncoder(encoder_layers, num_layers)
    self.final_layer = nn.Linear(d_model, d_model)

    self.init_weights()

  def init_weights(self):
    initrange = 0.1
    self.embedding.weight.data.uniform_(-initrange, initrange)
    self.expander.bias.data.zero_()
    self.expander.weight.data.uniform_(-initrange, initrange)
    self.final_layer.bias.data.zero_()
    self.final_layer.weight.data.uniform_(-initrange, initrange)

  def forward(self, src):
    # handle mask
    src_key_padding_mask = src.eq(np.inf)     # (B, S+A)

    # special token
    dummy = torch.zeros((src.size(0), 1), dtype=torch.long).to(src.device)    # (B, 1)
    cls = self.embedding(dummy).transpose(0, 1)                               # (1, B, D)

    # input
    src[src == np.inf] = 0                    # (B, S+A)
    src = src.permute(1, 0).unsqueeze(-1)     # (S+A, B, 1)
    src = self.expander(src)                  # (S+A, B, D)

    # concat
    src = torch.cat((cls, src), dim=0)        # (S+A+1, B, D)
    src_key_padding_mask = torch.cat(
      (dummy.to(dtype=torch.bool), src_key_padding_mask), dim=1)    # (B, S+A+1)

    # output
    output = self.transformer_encoder(src, src_key_padding_mask=src_key_padding_mask)
    output = self.final_layer(output[0])                            # (B, D)
    output = torch.matmul(output, self.embedding.weight.t())        # (B, 1)

    return output