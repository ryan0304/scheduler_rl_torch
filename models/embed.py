import torch
import torch.nn as nn
import torch.nn.functional as F


class StateEmbedding(nn.Module):

  def __init__(self, d_model):
    super(StateEmbedding, self).__init__()
    grid_size = 5
    coordinates = [(x, y) for x in range(grid_size) for y in range(grid_size)]
    self.coord_dic = {x: i+1 for i, x in enumerate(coordinates)}
    self.out = 0

    num_embeddings = len(self.coord_dic) + 1
    self.embedding = nn.Embedding(num_embeddings, d_model)

    self.init_weights()

  def coordinate_to_id(self, obs):
    device = obs.device
    coordinate = obs.tolist()
    id_ = [self.coord_dic.setdefault(tuple(map(int, x)), self.out) for x in coordinate]
    id_ = torch.LongTensor(id_, device=device)
    return id_

  def forward(self, coordinate_state):
    id_tensor = self.coordinate_to_id(coordinate_state)   # (B, 2) --> (B,)
    embedded = self.embedding(id_tensor)                  # (B,) --> (B, d_model)
    return embedded

  def init_weights(self):
    initrange = 0.1
    self.embedding.weight.data.uniform_(-initrange, initrange)


class ActionEmbedding(nn.Module):

  def __init__(self, d_model):
    super(ActionEmbedding, self).__init__()
    num_embeddings = 4
    self.embedding = nn.Embedding(num_embeddings, d_model)

    self.init_weights()

  def forward(self, action_state):
    id_tensor = torch.argmax(action_state, dim=-1)    # (B, A) --> (B,)
    embedded = self.embedding(id_tensor)      # (B,) --> (B, d_model)
    return embedded

  def init_weights(self):
    initrange = 0.1
    self.embedding.weight.data.uniform_(-initrange, initrange)


class EmbeddingWrapper(nn.Module):

  def __init__(self, d_model):
    super(EmbeddingWrapper, self).__init__()
    self.d_model = d_model
    self.state_embedder = StateEmbedding(d_model)
    self.action_embedder = ActionEmbedding(d_model)

  def state_to_embedding(self, state):
    # state: (B, 2)
    return self.state_embedder(state)

  def action_to_embedding(self, action):
    # action: (B, 2) or (B, 4)
    return self.action_embedder(action)


class EmbeddingCritic(nn.Module):

  def __init__(self, embedder):
    super(EmbeddingCritic, self).__init__()
    d_model = embedder.d_model
    self.embedder = embedder
    self.dense1 = nn.Linear(d_model*2, d_model*2)
    self.dense2 = nn.Linear(d_model*2, d_model*2)
    self.out = nn.Linear(d_model*2, 1)

    self.init_weights()

  def forward(self, src):
    state = src[:, :2].clone()                          # (B, 2)
    action = src[:, 2:].clone()                         # (B, 2) or (B, 4)
    state = self.embedder.state_to_embedding(state)     # (B, d_model)
    action = self.embedder.action_to_embedding(action)  # (B, d_model)
    merged = torch.cat((state, action), dim=-1)         # (B, d_model*2)

    output = F.relu(self.dense1(merged))        # (B, d_model)
    output = F.relu(self.dense2(output))        # (B, d_model)
    output = self.out(output)                   # (B, action_dim)

    return output

  def init_weights(self):
    initrange = 0.1
    self.dense1.bias.data.zero_()
    self.dense1.weight.data.uniform_(-initrange, initrange)
    self.dense2.bias.data.zero_()
    self.dense2.weight.data.uniform_(-initrange, initrange)
    self.out.bias.data.zero_()
    self.out.weight.data.uniform_(-initrange, initrange)


class EmbeddingActor(nn.Module):

  def __init__(self, embedder, action_dim):
    super(EmbeddingActor, self).__init__()
    d_model = embedder.d_model
    self.embedder = embedder
    self.action_dim = action_dim
    self.dense1 = nn.Linear(d_model, d_model)
    self.dense2 = nn.Linear(d_model, d_model)
    self.out = nn.Linear(d_model, action_dim)

    self.init_weights()

  def forward(self, src):
    # embedding: (B, 2) --> (B, d_model)
    state = self.embedder.state_to_embedding(src)
    logit = F.relu(self.dense1(state))      # (B, d_model)
    logit = F.relu(self.dense2(logit))      # (B, d_model)
    logit = self.out(logit)                 # (B, action_dim)

    return logit

  def init_weights(self):
    initrange = 0.1
    self.dense1.bias.data.zero_()
    self.dense1.weight.data.uniform_(-initrange, initrange)
    self.dense2.bias.data.zero_()
    self.dense2.weight.data.uniform_(-initrange, initrange)
    self.out.bias.data.zero_()
    self.out.weight.data.uniform_(-initrange, initrange)


class EmbeddingValue(nn.Module):

  def __init__(self, embedder):
    super(EmbeddingValue, self).__init__()
    d_model = embedder.d_model
    self.embedder = embedder
    self.dense1 = nn.Linear(d_model, d_model)
    self.dense2 = nn.Linear(d_model, d_model)
    self.out = nn.Linear(d_model, 1)

    self.init_weights()

  def forward(self, src):
    # embedding: (B, 2) --> (B, d_model)
    state = self.embedder.state_to_embedding(src)
    output = F.relu(self.dense1(state))       # (B, d_model)
    output = F.relu(self.dense2(output))      # (B, d_model)
    output = self.out(output)                 # (B, A)

    return output

  def init_weights(self):
    initrange = 0.1
    self.dense1.bias.data.zero_()
    self.dense1.weight.data.uniform_(-initrange, initrange)
    self.dense2.bias.data.zero_()
    self.dense2.weight.data.uniform_(-initrange, initrange)
    self.out.bias.data.zero_()
    self.out.weight.data.uniform_(-initrange, initrange)
