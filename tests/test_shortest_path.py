import pdb
import sys
import torch

sys.path.append('..')

from config import Configs
from agent import TD3Agent
from envs import ShortestPath


agent = TD3Agent(**Configs.TD3ConfigBase.to_dict())
agent.load_model('../checkpoints/first.pt')
agent.train(False)

env = ShortestPath(**Configs.ShortestPathConfig.to_dict())


def tt(x):
  x = torch.FloatTensor(x).to('cuda:0')
  x = x.unsqueeze(0)
  return x


def aa(observation):
  observation = tt(observation)
  distribution = agent.actor(observation)
  return distribution


def cc(observation, action):
  observation = tt(observation)
  action = tt(action)
  q1, q2 = agent.critic(observation, action)
  return q1, q2


if __name__ == "__main__":
  obs = [3, 4]
  act1 = [0, 1, 0, 0]
  act2 = [0, 0, 0, 1]

  dist = aa(obs)
  q11, q12 = cc(obs, act1)
  q21, q22 = cc(obs, act2)

  pdb.set_trace()
