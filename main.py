from absl import app, flags

from config import Configs
from train.trainer import Trainer


FLAGS = flags.FLAGS

flags.DEFINE_boolean('debug', False, 'Produces debugging output.')
flags.DEFINE_boolean('load', False, 'Load model & replay buffer')


def main(argv):
  if FLAGS.debug:
    print('non-flag arguments:', argv)

  trainer = Trainer(**Configs.TrainConfig.to_dict())

  if FLAGS.load:
    trainer.agent.load_model()
    trainer.replay_buffer.load()

  trainer.fit()
  return


if __name__ == '__main__':
  app.run(main)